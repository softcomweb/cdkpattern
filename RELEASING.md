# Release a new version

This document outlines the steps to release a new version of the packages. You can skip it if you are not a developer.

## Prerequisites

All tests pass. Update the `version` property in `package.json` according to [Semantic Versioning](https://semver.org/lang/de/).

## Step 1. Run Build

```
cd packages/@softcomweb-ec/mypkg
npm version patch
npm install
npm run test
npm run build
npm run docgen # generates API.md
npm run build
rm -rf dist
npm run package
```

This will create a folder `dist` which is used for publishing.

OR

```
./release.sh
```

## Step 2. Create a Tag

Run the following

```
SEM_VERSION="<replace-me>"
GIT_SHA=`git rev-parse HEAD`
git tag -a "v${SEM_VERSION}" ${GIT_SHA} -m "release ${SEM_VERSION}"  
git push origin "v${SEM_VERSION}"
```

## Step 3. Publish package

To publish for NPM

```
cd packages/@softcomweb-ec/mypkg
npm run release:npm
```

To publish for Maven

```
cd packages/@softcomweb-ec/mypkg
npm run release:maven
```
