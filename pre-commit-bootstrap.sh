#!/usr/bin/env bash

set -eo pipefail

echo "[INFO] Installing dependencies for detect-secrets pre-commit hook"

if [ -z "$(which pre-commit)" ]; then
  pip install pre-commit
fi

if [ -z "$(which detect-secrets)" ]; then
  pip install detect-secrets
fi

echo "[INFO] Running pre-commit install"
pre-commit install

echo "[INFO] Done..."
