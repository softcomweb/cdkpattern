import { Construct } from "constructs";
//import { App } from "aws-cdk-lib";

const FEATURE_FLAG_SERVICE_IDENTITY = "@softcomweb-ec/cdkpatterns_microservice:serviceIdentity";
const FEATURE_FLAG_SERVICE_OWNER = "@softcomweb-ec/cdkpatterns_microservice:serviceOwner";
const FEATURE_FLAG_SERVICE_STAGE = "@softcomweb-ec/cdkpatterns_microservice:serviceStage";

export interface MicroserviceBaseProps {
  /**
   * Name of the application. This is used in alarms and other
   * namings. This is the sub-component of your service, e.g.
   * if you have service "orderprocessing" (serviceIdentity) and this
   * microservice sends emails, set this property to "emailsender".
   *
   * @default the id of the construct
   */
  readonly serviceName?: string;
  /**
   * The identity of the service. Together with serviceName
   * this will form the whole identity of your service. Use this
   * to standardize all resources to a certain namespace.
   *
   * There is no standard, but we recommend a FQDN like
   * "order.cloud.softcomweb.de".
   *
   * @default the id of the CDK node
   */
  readonly serviceIdentity?: string;
  /**
   * The stage (or environment) your service. Set this
   * to distinduish between multiple staged deployments.
   *
   * @default default
   */
  readonly serviceStage?: string;
  /**
   * The owner of the service. This is used to identity
   * who is responsible. Just for information purposes.
   *
   * @default unknown
   */
  readonly serviceOwner?: string;
  /**
   * If set to true, all resources within this construct have fixed
   * names. Attention: This is generally an anti-pattern as some
   * resources cannot be properly renamed or updated.
   *
   * @default false
   */
  readonly createNamedResources?: boolean;
}

export interface CreateNameProps {
  readonly includeServiceIdentity?: boolean;
}

export class MicroserviceBase extends Construct {
  readonly serviceName: string;
  readonly serviceIdentity: string;
  readonly serviceStage: string;
  readonly serviceOwner: string;
  readonly createNamedResources?: boolean;

  constructor(scope: Construct, id: string, props: MicroserviceBaseProps) {
    super(scope, id);

    this.serviceIdentity =
      props.serviceIdentity || this.node.tryGetContext(FEATURE_FLAG_SERVICE_IDENTITY) || scope.node.id;
    this.serviceName = props.serviceName || id;
    this.serviceStage = props.serviceStage || this.node.tryGetContext(FEATURE_FLAG_SERVICE_STAGE) || "default";
    this.serviceOwner = props.serviceOwner || this.node.tryGetContext(FEATURE_FLAG_SERVICE_OWNER) || "unknown";
    this.createNamedResources = props.createNamedResources || false;
  }

  /**
   *
   * @param name the name of the result. will be appended.
   * @param delimiter the delimiter to use for joining the parts. defaults to '-' as it fits for most AWS resources
   * @returns
   */
  protected createSanitizedName(name: string, delimiter?: string): string | undefined {
    const sanitize = (x: string) => {
      return x.replace(/\./g, delimiter || "-").replace(/\//g, delimiter || "-");
    };
    return this.createNamedResources ? sanitize(name) : undefined;
  }

  /**
   * Create an SSM compatible parameter name.
   * @param name
   * @returns
   */
  protected createParameterName(name: string): string | undefined {
    return this.createNamedResources
      ? "/applications/" + [this.serviceIdentity, this.serviceName, this.serviceStage, name].join("/")
      : undefined;
  }

  /**
   * Create a custom CloudWatch log group name
   * @param name
   * @returns
   */
  protected createLogGroupName(name: string): string | undefined {
    return this.createParameterName(name);
  }

  /**
   * Create a CloudWatch alarm name
   * @param alarmId
   * @returns
   */
  protected createAlarmName(alarmId: string): string | undefined {
    return this.createName(alarmId, "/");
  }

  protected createFullName(id: string): string | undefined {
    return this.createName(id, "-");
  }

  protected createRegularNameWithoutIdentity(id: string): string | undefined {
    return this.createName(id, "-", {
      includeServiceIdentity: false,
    });
  }

  protected createName(id: string, delimiter: string, props?: CreateNameProps): string | undefined {
    const sanitize = (x: string) => {
      return x.replace(/\./g, delimiter);
    };
    const parts = [];
    if (props?.includeServiceIdentity) {
      parts.push(sanitize(this.serviceIdentity));
    }
    parts.push(sanitize(this.serviceName));
    parts.push(sanitize(this.serviceStage));
    parts.push(sanitize(id));

    return this.createNamedResources ? parts.join(delimiter) : undefined;
  }
}
