import * as cloudwatch from "aws-cdk-lib/aws-cloudwatch";
import * as events from "aws-cdk-lib/aws-events";
import * as eventstargets from "aws-cdk-lib/aws-events-targets";
import { Construct } from "constructs";
import { LambdaMicroservice, LambdaMicroserviceProps } from "./microservice-lambda";

export interface LambdaEventedMicroserviceProps extends LambdaMicroserviceProps {
  readonly eventPattern: events.EventPattern;
  readonly eventBus?: events.IEventBus;
  readonly eventRuleEnabled?: boolean;
}

export class LambdaEventedMicroservice extends LambdaMicroservice {
  public readonly rule: events.Rule;

  constructor(scope: Construct, id: string, props: LambdaEventedMicroserviceProps) {
    super(scope, id, props);

    this.rule = new events.Rule(this, "handlerTrigger", {
      description: `Event rule to trigger lambda function ${this.handler.functionName}`,
      enabled: props.eventRuleEnabled || true,
      eventPattern: props.eventPattern,
      eventBus: props.eventBus,
      targets: [new eventstargets.LambdaFunction(this.handler)],
    });

    this.handlerAlarms.push(
      new cloudwatch.Alarm(this, "ruleFailedAlarm", {
        evaluationPeriods: 1,
        metric: new cloudwatch.Metric({
          namespace: "AWS/Events",
          metricName: "FailedInvocations",
          dimensionsMap: {
            RuleName: this.rule.ruleName,
          },
        }),
        threshold: 1,
        alarmDescription: [
          "MEDIUM",
          `EventBridge rule ${this.rule.ruleName} has failed invocations`,
          "This may lead to missing lambda invocations",
          "Look at the Lambda logs and fix the implementation",
        ].join(" | "),
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),
    );

    this.handlerAlarms.push(
      new cloudwatch.Alarm(this, "ruleThrottlesAlarm", {
        evaluationPeriods: 1,
        metric: new cloudwatch.Metric({
          namespace: "AWS/Events",
          metricName: "ThrottledRules",
          dimensionsMap: {
            RuleName: this.rule.ruleName,
          },
        }),
        threshold: 1,
        alarmDescription: [
          "HIGH",
          `EventBridge rule ${this.rule.ruleName} is being throttled`,
          "This may lead to missing lambda invocations",
          "Look for infinite loops in the event configuration",
        ].join(" | "),
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),
    );
  }
}
