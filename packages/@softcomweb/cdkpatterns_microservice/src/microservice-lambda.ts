import * as lambdapython from "@aws-cdk/aws-lambda-python-alpha";
import * as cdk from "aws-cdk-lib";
import * as cloudwatch from "aws-cdk-lib/aws-cloudwatch";
import * as codedeploy from "aws-cdk-lib/aws-codedeploy";
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as iam from "aws-cdk-lib/aws-iam";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as logs from "aws-cdk-lib/aws-logs";
import { Construct } from "constructs";
import { MicroserviceBase, MicroserviceBaseProps } from "./microservice-base";

export interface LambdaMicroserviceProps extends MicroserviceBaseProps {
  /**
   * FQDN of the Lambda handler. See the documentation for respective runtime.
   */
  readonly handler: string;
  /**
   * Lambda code. This is required if pythonFunctionProps is not set.
   */
  readonly handlerCodeAsset?: lambda.Code;
  /**
   * Runtime of the handler
   */
  readonly handlerRuntime: lambda.Runtime;
  /**
   * Timeout of the handler.
   *
   * @default 3 seconds
   */
  readonly handlerTimeout?: cdk.Duration;
  /**
   * Memory size
   *
   * @default 128
   */
  readonly handlerMemorySize?: number;
  /**
   * Deployment strategy for CodeDeploy.
   *
   * @default codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE
   */
  readonly handlerDeploymentStrategy?: codedeploy.ILambdaDeploymentConfig;
  /**
   * Deployment hook before traffic is routed to the Lambda function.
   *
   * @default undefined
   */
  readonly handlerDeploymentPostTrafficHook?: lambda.IFunction;
  /**
   * Deployment hook after traffic is routed to the Lambda function.
   *
   * @default undefined
   */
  readonly handlerDeploymentPreTrafficHook?: lambda.IFunction;
  /**
   * VPC to put the Lambda to.
   *
   * @default undefined
   */
  readonly handlerVpc?: ec2.IVpc;
  /**
   * Subnets to use when putting Lambda to the VPC.
   *
   * @default - all subnets
   */
  readonly handlerVpcSubnets?: ec2.SubnetSelection;
  /**
   * Security groups to attach to the Lambda function.
   *
   * @default - a new security group is created
   */
  readonly handlerVpcSecurityGroups?: ec2.ISecurityGroup[];
  /**
   * Lambda layers to add to the Lambda.
   *
   * @default - no layers are added
   */
  readonly handlerLayers?: lambda.ILayerVersion[];
  /**
   * Log retention for Lambda logs.
   *
   * @default - 7 days
   */
  readonly handlerLogRetention?: logs.RetentionDays;
  /**
   * IAM policies for the handler.
   *
   * @default undefined
   */
  readonly handlerInitialPolicy?: iam.PolicyStatement[];
  /**
   * IAM policies for the handler.
   *
   * @default undefined
   */
  readonly handlerInsightsVersion?: lambda.LambdaInsightsVersion;
  /**
   * Root path of the Lambda handler code. If this contains one of "requirements.txt",
   * "Pipfile" or "pyproject.toml", the bundling runtime will also install
   * all dependencies.
   */
  readonly pythonEntry?: string;
  /**
   * Index file for Python lambdas.
   *
   * @default handler.py
   */
  readonly pythonIndex?: string;
  /**
   * Use an existing Lambda function instead of creating
   * a new one. The rest of the setup (env variables, logs etc)
   * is taken care of.
   */
  readonly existingFunction?: lambda.Function;
}

export class LambdaMicroservice extends MicroserviceBase implements iam.IGrantable {
  public readonly handler: lambda.Function;
  public readonly handlerAlias: lambda.Alias;
  public readonly handlerLogs: logs.LogGroup;
  public readonly handlerDeploymentGroup: codedeploy.LambdaDeploymentGroup;
  public readonly handlerAlarms: cloudwatch.IAlarm[] = [];
  public readonly grantPrincipal: iam.IPrincipal;

  constructor(scope: Construct, id: string, props: LambdaMicroserviceProps) {
    super(scope, id, props);

    const handlerEnvironment: Record<string, string> = {
      POWERTOOLS_SERVICE_NAME: this.serviceName,
      POWERTOOLS_METRICS_NAMESPACE: `${this.serviceIdentity}/${this.serviceName}`,
      POWERTOOLS_TRACE_DISABLED: "false",
      POWERTOOLS_TRACER_CAPTURE_RESPONSE: "true",
      POWERTOOLS_TRACER_CAPTURE_ERROR: "true",
      POWERTOOLS_LOGGER_LOG_EVENT: "true",
      POWERTOOLS_LOGGER_SAMPLE_RATE: "0",
      LOG_LEVEL: "INFO",
      SERVICE_IDENTITY: this.serviceIdentity,
      SERVICE_NAME: this.serviceName,
      SERVICE_STAGE: this.serviceStage,
    };

    const handlerProps = {
      functionName: this.createFullName("handler"),
      code: props.handlerCodeAsset!,
      handler: props.handler,
      runtime: props.handlerRuntime,
      tracing: lambda.Tracing.ACTIVE,
      description: `[${this.serviceIdentity}/${this.serviceName}] ${this.node.addr}`,
      environment: handlerEnvironment,
      memorySize: props.handlerMemorySize,
      timeout: props.handlerTimeout,
      vpc: props.handlerVpc,
      vpcSubnets: props.handlerVpcSubnets,
      allowAllOutbound: props.handlerVpc ? true : undefined,
      securityGroups: props.handlerVpcSecurityGroups,
      initialPolicy: props.handlerInitialPolicy,
      insightsVersion: props.handlerInsightsVersion,
    };

    this.handler =
      props.existingFunction ||
      (() => {
        if (props.handlerRuntime.family === lambda.RuntimeFamily.PYTHON && props.pythonEntry) {
          return new lambdapython.PythonFunction(this, "handler", {
            entry: props.pythonEntry,
            index: props.pythonIndex || "handler.py",
            ...handlerProps,
            handler: props.handler.split(".").pop(),
          });
        }
        return new lambda.Function(this, "handler", {
          ...handlerProps,
        });
      })();

    this.grantPrincipal = this.handler.grantPrincipal;

    if (props.existingFunction) {
      Object.keys(handlerEnvironment).forEach(envKey => {
        this.handler.addEnvironment(envKey, handlerEnvironment[envKey]!);
      });
    }

    if (props.handlerLayers) {
      props.handlerLayers.forEach(layer => this.handler.addLayers(layer));
    }

    this.handlerAlias = this.handler.addAlias("prod");

    this.handlerLogs = new logs.LogGroup(this, "handlerLogs", {
      logGroupName: `/aws/lambda/${this.handler.functionName}`,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      retention: props.handlerLogRetention || logs.RetentionDays.ONE_WEEK,
    });

    this.handlerAlarms.push(
      new cloudwatch.Alarm(this, "handlerErrorsAlarm", {
        alarmName: this.createAlarmName("handlerErrorsAlarm"),
        evaluationPeriods: 1,
        metric: this.handler.metricErrors(),
        threshold: 1,
        alarmDescription: [
          "HIGH",
          `Lambda function ${this.handler.functionName} has errors`,
          "This may lead to decreased availability",
          `Look at the CloudWatch log group ${this.handlerLogs.logGroupName} and fix the implementation`,
        ].join(" | "),
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),
    );

    this.handlerAlarms.push(
      new cloudwatch.Alarm(this, "handlerThrottlesAlarm", {
        alarmName: this.createAlarmName("handlerThrottlesAlarm"),
        evaluationPeriods: 1,
        metric: this.handler.metricErrors(),
        threshold: 1,
        alarmDescription: [
          "HIGH",
          `Lambda function ${this.handler.functionName} is being throttled`,
          "This may lead to decreased availability",
          `Look at the CloudWatch log group ${this.handlerLogs.logGroupName} and fix the implementation`,
        ].join(" | "),
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),
    );

    this.handlerDeploymentGroup = new codedeploy.LambdaDeploymentGroup(this, "handlerDeploymentGroup", {
      alias: this.handlerAlias,
      deploymentConfig: props.handlerDeploymentStrategy || codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE,
      postHook: props.handlerDeploymentPostTrafficHook,
      preHook: props.handlerDeploymentPreTrafficHook,
      alarms: this.handlerAlarms,
    });
  }
}
