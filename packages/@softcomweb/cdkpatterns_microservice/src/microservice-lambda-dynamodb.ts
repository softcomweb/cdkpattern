import * as cdk from "aws-cdk-lib";
import * as cloudwatch from "aws-cdk-lib/aws-cloudwatch";
import * as dynamodb from "aws-cdk-lib/aws-dynamodb";
import { Construct } from "constructs";
import { LambdaMicroservice, LambdaMicroserviceProps } from "./microservice-lambda";

export interface LambdaMicroserviceDynamoDBProps extends LambdaMicroserviceProps {
  /**
   * An existing table. If not set, a new DynamoDB table will be created.
   *
   * @default a new table is created
   */
  readonly table?: dynamodb.ITable;
  /**
   * Name of the environment variable to inject into the Lambda environment.
   *
   * @default DYNAMODB_TABLE_NAME
   */
  readonly tableNameEnvironmentVariable?: string;
}

/**
 * A CDK construct for a Lambda microservice with DynamoDB table.
 */
export class LambdaMicroserviceDynamoDB extends LambdaMicroservice {
  public readonly table: dynamodb.ITable;
  public readonly tableThrottledRequestsAlarm?: cloudwatch.Alarm;
  public readonly tableSystemErrorsAlarm?: cloudwatch.Alarm;

  constructor(scope: Construct, id: string, props: LambdaMicroserviceDynamoDBProps) {
    super(scope, id, props);

    this.table =
      props.table ||
      new dynamodb.Table(this, "table", {
        partitionKey: {
          name: "pk",
          type: dynamodb.AttributeType.STRING,
        },
        sortKey: {
          name: "sk",
          type: dynamodb.AttributeType.STRING,
        },
        billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        removalPolicy: cdk.RemovalPolicy.DESTROY,
        encryption: dynamodb.TableEncryption.AWS_MANAGED,
        timeToLiveAttribute: "ttl",
        tableName: this.createSanitizedName("table", "-"),
        contributorInsightsEnabled: false,
        pointInTimeRecovery: true,
      });

    this.table.grantReadWriteData(this);
    this.handler.addEnvironment(props.tableNameEnvironmentVariable || "DYNAMODB_TABLE_NAME", this.table.tableName);

    if (props.table === undefined) {
      this.tableThrottledRequestsAlarm = new cloudwatch.Alarm(this, "tableThrottledRequestsAlarm", {
        alarmName: this.createAlarmName("tableThrottledRequestsAlarm"),
        evaluationPeriods: 1,
        metric: this.table.metricThrottledRequests(),
        threshold: 1,
        alarmDescription: [
          "CRITICAL",
          `DynamoDB table ${this.table.tableName} throttles requests`,
          "This may lead to decreased availability",
          `Look at the CloudWatch metrics for the table ${this.table.tableName} and check for excessive usage. You may also increase the read/write capacity.`,
        ].join(" | "),
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      });

      this.tableSystemErrorsAlarm = new cloudwatch.Alarm(this, "tableSystemErrorsAlarm", {
        alarmName: this.createAlarmName("tableSystemErrorsAlarm"),
        evaluationPeriods: 1,
        metric: this.table.metricSystemErrorsForOperations(),
        threshold: 1,
        alarmDescription: [
          "CRITICAL",
          `DynamoDB table ${this.table.tableName} returns 5xx system errors`,
          "This may lead to decreased availability",
          `Look at the PHD for ongoing issues with DynamoDB service.`,
        ].join(" | "),
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      });
    }
  }
}
