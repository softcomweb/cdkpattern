import * as cdk from "aws-cdk-lib";
import * as lambda from "aws-cdk-lib/aws-lambda";
import { LambdaMicroservice } from "../microservice-lambda";

const app = new cdk.App();
const stack = new cdk.Stack(app, "cdkpatterns-microservice-integrationtest");

new LambdaMicroservice(stack, "ms-python", {
  serviceIdentity: "cdkpatterns-microservice-integrationtest",
  serviceName: "ms-python",
  handler: "handler.handle_request",
  handlerRuntime: lambda.Runtime.PYTHON_3_9,
  pythonEntry: `${__dirname}/lambda.d/python`,
  pythonIndex: "handler.py",
});

if (!module.children) {
  app.synth();
}
