# API Reference

**Classes**

Name|Description
----|-----------
[LambdaEventedMicroservice](#softcomweb-ec-cdkpatterns-microservice-lambdaeventedmicroservice)|*No description*
[LambdaMicroservice](#softcomweb-ec-cdkpatterns-microservice-lambdamicroservice)|*No description*
[LambdaScheduledMicroservice](#softcomweb-ec-cdkpatterns-microservice-lambdascheduledmicroservice)|*No description*
[MicroserviceBase](#softcomweb-ec-cdkpatterns-microservice-microservicebase)|*No description*


**Structs**

Name|Description
----|-----------
[CreateNameProps](#softcomweb-ec-cdkpatterns-microservice-createnameprops)|*No description*
[LambdaEventedMicroserviceProps](#softcomweb-ec-cdkpatterns-microservice-lambdaeventedmicroserviceprops)|*No description*
[LambdaMicroserviceProps](#softcomweb-ec-cdkpatterns-microservice-lambdamicroserviceprops)|*No description*
[LambdaScheduledMicroserviceProps](#softcomweb-ec-cdkpatterns-microservice-lambdascheduledmicroserviceprops)|*No description*
[MicroserviceBaseProps](#softcomweb-ec-cdkpatterns-microservice-microservicebaseprops)|*No description*



## class LambdaEventedMicroservice  <a id="softcomweb-ec-cdkpatterns-microservice-lambdaeventedmicroservice"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable), [IGrantable](#aws-cdk-aws-iam-igrantable)
__Extends__: [LambdaMicroservice](#softcomweb-ec-cdkpatterns-microservice-lambdamicroservice)

### Initializer




```ts
new LambdaEventedMicroservice(scope: Construct, id: string, props: LambdaEventedMicroserviceProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[LambdaEventedMicroserviceProps](#softcomweb-ec-cdkpatterns-microservice-lambdaeventedmicroserviceprops)</code>)  *No description*
  * **createNamedResources** (<code>boolean</code>)  *No description* __*Optional*__
  * **serviceIdentity** (<code>string</code>)  *No description* __*Optional*__
  * **serviceName** (<code>string</code>)  *No description* __*Optional*__
  * **serviceOwner** (<code>string</code>)  *No description* __*Optional*__
  * **serviceStage** (<code>string</code>)  *No description* __*Optional*__
  * **handler** (<code>string</code>)  FQDN of the Lambda handler.
  * **handlerRuntime** (<code>[Runtime](#aws-cdk-aws-lambda-runtime)</code>)  Runtime of the handler.
  * **existingFunction** (<code>[Function](#aws-cdk-aws-lambda-function)</code>)  Use an existing Lambda function instead of creating a new one. __*Optional*__
  * **handlerCodeAsset** (<code>[Code](#aws-cdk-aws-lambda-code)</code>)  Lambda code. __*Optional*__
  * **handlerDeploymentPostTrafficHook** (<code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code>)  Deployment hook before traffic is routed to the Lambda function. __*Default*__: undefined
  * **handlerDeploymentPreTrafficHook** (<code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code>)  Deployment hook after traffic is routed to the Lambda function. __*Default*__: undefined
  * **handlerDeploymentStrategy** (<code>[ILambdaDeploymentConfig](#aws-cdk-aws-codedeploy-ilambdadeploymentconfig)</code>)  Deployment strategy for CodeDeploy. __*Default*__: codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE
  * **handlerInitialPolicy** (<code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code>)  IAM policies for the handler. __*Default*__: undefined
  * **handlerInsightsVersion** (<code>[LambdaInsightsVersion](#aws-cdk-aws-lambda-lambdainsightsversion)</code>)  IAM policies for the handler. __*Default*__: undefined
  * **handlerLayers** (<code>Array<[ILayerVersion](#aws-cdk-aws-lambda-ilayerversion)></code>)  Lambda layers to add to the Lambda. __*Default*__: no layers are added
  * **handlerLogRetention** (<code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code>)  Log retention for Lambda logs. __*Default*__: 7 days
  * **handlerMemorySize** (<code>number</code>)  Memory size. __*Default*__: 128
  * **handlerTimeout** (<code>[Duration](#aws-cdk-core-duration)</code>)  Timeout of the handler. __*Default*__: 3 seconds
  * **handlerVpc** (<code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code>)  VPC to put the Lambda to. __*Default*__: undefined
  * **handlerVpcSecurityGroups** (<code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code>)  Security groups to attach to the Lambda function. __*Default*__: a new security group is created
  * **handlerVpcSubnets** (<code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code>)  Subnets to use when putting Lambda to the VPC. __*Default*__: all subnets
  * **pythonEntry** (<code>string</code>)  Root path of the Lambda handler code. __*Optional*__
  * **pythonIndex** (<code>string</code>)  Index file for Python lambdas. __*Default*__: handler.py
  * **eventPattern** (<code>[EventPattern](#aws-cdk-aws-events-eventpattern)</code>)  *No description*
  * **eventBus** (<code>[IEventBus](#aws-cdk-aws-events-ieventbus)</code>)  *No description* __*Optional*__
  * **eventRuleEnabled** (<code>boolean</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description
-----|------|-------------
**rule** | <code>[Rule](#aws-cdk-aws-events-rule)</code> | <span></span>



## class LambdaMicroservice  <a id="softcomweb-ec-cdkpatterns-microservice-lambdamicroservice"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable), [IGrantable](#aws-cdk-aws-iam-igrantable)
__Extends__: [MicroserviceBase](#softcomweb-ec-cdkpatterns-microservice-microservicebase)

### Initializer




```ts
new LambdaMicroservice(scope: Construct, id: string, props: LambdaMicroserviceProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[LambdaMicroserviceProps](#softcomweb-ec-cdkpatterns-microservice-lambdamicroserviceprops)</code>)  *No description*
  * **createNamedResources** (<code>boolean</code>)  *No description* __*Optional*__
  * **serviceIdentity** (<code>string</code>)  *No description* __*Optional*__
  * **serviceName** (<code>string</code>)  *No description* __*Optional*__
  * **serviceOwner** (<code>string</code>)  *No description* __*Optional*__
  * **serviceStage** (<code>string</code>)  *No description* __*Optional*__
  * **handler** (<code>string</code>)  FQDN of the Lambda handler.
  * **handlerRuntime** (<code>[Runtime](#aws-cdk-aws-lambda-runtime)</code>)  Runtime of the handler.
  * **existingFunction** (<code>[Function](#aws-cdk-aws-lambda-function)</code>)  Use an existing Lambda function instead of creating a new one. __*Optional*__
  * **handlerCodeAsset** (<code>[Code](#aws-cdk-aws-lambda-code)</code>)  Lambda code. __*Optional*__
  * **handlerDeploymentPostTrafficHook** (<code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code>)  Deployment hook before traffic is routed to the Lambda function. __*Default*__: undefined
  * **handlerDeploymentPreTrafficHook** (<code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code>)  Deployment hook after traffic is routed to the Lambda function. __*Default*__: undefined
  * **handlerDeploymentStrategy** (<code>[ILambdaDeploymentConfig](#aws-cdk-aws-codedeploy-ilambdadeploymentconfig)</code>)  Deployment strategy for CodeDeploy. __*Default*__: codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE
  * **handlerInitialPolicy** (<code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code>)  IAM policies for the handler. __*Default*__: undefined
  * **handlerInsightsVersion** (<code>[LambdaInsightsVersion](#aws-cdk-aws-lambda-lambdainsightsversion)</code>)  IAM policies for the handler. __*Default*__: undefined
  * **handlerLayers** (<code>Array<[ILayerVersion](#aws-cdk-aws-lambda-ilayerversion)></code>)  Lambda layers to add to the Lambda. __*Default*__: no layers are added
  * **handlerLogRetention** (<code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code>)  Log retention for Lambda logs. __*Default*__: 7 days
  * **handlerMemorySize** (<code>number</code>)  Memory size. __*Default*__: 128
  * **handlerTimeout** (<code>[Duration](#aws-cdk-core-duration)</code>)  Timeout of the handler. __*Default*__: 3 seconds
  * **handlerVpc** (<code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code>)  VPC to put the Lambda to. __*Default*__: undefined
  * **handlerVpcSecurityGroups** (<code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code>)  Security groups to attach to the Lambda function. __*Default*__: a new security group is created
  * **handlerVpcSubnets** (<code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code>)  Subnets to use when putting Lambda to the VPC. __*Default*__: all subnets
  * **pythonEntry** (<code>string</code>)  Root path of the Lambda handler code. __*Optional*__
  * **pythonIndex** (<code>string</code>)  Index file for Python lambdas. __*Default*__: handler.py



### Properties


Name | Type | Description
-----|------|-------------
**grantPrincipal** | <code>[IPrincipal](#aws-cdk-aws-iam-iprincipal)</code> | The principal to grant permissions to.
**handler** | <code>[Function](#aws-cdk-aws-lambda-function)</code> | <span></span>
**handlerAlarms** | <code>Array<[IAlarm](#aws-cdk-aws-cloudwatch-ialarm)></code> | <span></span>
**handlerAlias** | <code>[Alias](#aws-cdk-aws-lambda-alias)</code> | <span></span>
**handlerDeploymentGroup** | <code>[LambdaDeploymentGroup](#aws-cdk-aws-codedeploy-lambdadeploymentgroup)</code> | <span></span>
**handlerLogs** | <code>[LogGroup](#aws-cdk-aws-logs-loggroup)</code> | <span></span>

### Methods


#### protected onPrepare() no longer available

The <code>onPrepare()</code> hook was used with CDKv1.x to perform final modifications before
synthesis, if necessary.

With CDKv2.x, using `constructs` instead of `@aws-cdk/core`, this feature has been
removed. As described in the [RFC](https://github.com/aws/aws-cdk-rfcs/blob/master/text/0192-remove-constructs-compat.md#06-no-prepare-the-prepare-hook-is-no-longer-supported),
one should use other features like e.g. Lazy to perform these kinds of modifications.

See the RFC for an example of how to emulate the previous behaviour, if
really necessary.


## class LambdaScheduledMicroservice  <a id="softcomweb-ec-cdkpatterns-microservice-lambdascheduledmicroservice"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable), [IGrantable](#aws-cdk-aws-iam-igrantable)
__Extends__: [LambdaMicroservice](#softcomweb-ec-cdkpatterns-microservice-lambdamicroservice)

### Initializer




```ts
new LambdaScheduledMicroservice(scope: Construct, id: string, props: LambdaScheduledMicroserviceProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[LambdaScheduledMicroserviceProps](#softcomweb-ec-cdkpatterns-microservice-lambdascheduledmicroserviceprops)</code>)  *No description*
  * **createNamedResources** (<code>boolean</code>)  *No description* __*Optional*__
  * **serviceIdentity** (<code>string</code>)  *No description* __*Optional*__
  * **serviceName** (<code>string</code>)  *No description* __*Optional*__
  * **serviceOwner** (<code>string</code>)  *No description* __*Optional*__
  * **serviceStage** (<code>string</code>)  *No description* __*Optional*__
  * **handler** (<code>string</code>)  FQDN of the Lambda handler.
  * **handlerRuntime** (<code>[Runtime](#aws-cdk-aws-lambda-runtime)</code>)  Runtime of the handler.
  * **existingFunction** (<code>[Function](#aws-cdk-aws-lambda-function)</code>)  Use an existing Lambda function instead of creating a new one. __*Optional*__
  * **handlerCodeAsset** (<code>[Code](#aws-cdk-aws-lambda-code)</code>)  Lambda code. __*Optional*__
  * **handlerDeploymentPostTrafficHook** (<code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code>)  Deployment hook before traffic is routed to the Lambda function. __*Default*__: undefined
  * **handlerDeploymentPreTrafficHook** (<code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code>)  Deployment hook after traffic is routed to the Lambda function. __*Default*__: undefined
  * **handlerDeploymentStrategy** (<code>[ILambdaDeploymentConfig](#aws-cdk-aws-codedeploy-ilambdadeploymentconfig)</code>)  Deployment strategy for CodeDeploy. __*Default*__: codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE
  * **handlerInitialPolicy** (<code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code>)  IAM policies for the handler. __*Default*__: undefined
  * **handlerInsightsVersion** (<code>[LambdaInsightsVersion](#aws-cdk-aws-lambda-lambdainsightsversion)</code>)  IAM policies for the handler. __*Default*__: undefined
  * **handlerLayers** (<code>Array<[ILayerVersion](#aws-cdk-aws-lambda-ilayerversion)></code>)  Lambda layers to add to the Lambda. __*Default*__: no layers are added
  * **handlerLogRetention** (<code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code>)  Log retention for Lambda logs. __*Default*__: 7 days
  * **handlerMemorySize** (<code>number</code>)  Memory size. __*Default*__: 128
  * **handlerTimeout** (<code>[Duration](#aws-cdk-core-duration)</code>)  Timeout of the handler. __*Default*__: 3 seconds
  * **handlerVpc** (<code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code>)  VPC to put the Lambda to. __*Default*__: undefined
  * **handlerVpcSecurityGroups** (<code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code>)  Security groups to attach to the Lambda function. __*Default*__: a new security group is created
  * **handlerVpcSubnets** (<code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code>)  Subnets to use when putting Lambda to the VPC. __*Default*__: all subnets
  * **pythonEntry** (<code>string</code>)  Root path of the Lambda handler code. __*Optional*__
  * **pythonIndex** (<code>string</code>)  Index file for Python lambdas. __*Default*__: handler.py
  * **eventSchedule** (<code>[Schedule](#aws-cdk-aws-events-schedule)</code>)  *No description*
  * **eventBus** (<code>[IEventBus](#aws-cdk-aws-events-ieventbus)</code>)  *No description* __*Optional*__
  * **eventRuleEnabled** (<code>boolean</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description
-----|------|-------------
**rule** | <code>[Rule](#aws-cdk-aws-events-rule)</code> | <span></span>



## class MicroserviceBase  <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable)
__Extends__: [Construct](#aws-cdk-core-construct)

### Initializer




```ts
new MicroserviceBase(scope: Construct, id: string, props: MicroserviceBaseProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[MicroserviceBaseProps](#softcomweb-ec-cdkpatterns-microservice-microservicebaseprops)</code>)  *No description*
  * **createNamedResources** (<code>boolean</code>)  *No description* __*Optional*__
  * **serviceIdentity** (<code>string</code>)  *No description* __*Optional*__
  * **serviceName** (<code>string</code>)  *No description* __*Optional*__
  * **serviceOwner** (<code>string</code>)  *No description* __*Optional*__
  * **serviceStage** (<code>string</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description
-----|------|-------------
**serviceIdentity** | <code>string</code> | <span></span>
**serviceName** | <code>string</code> | <span></span>
**serviceOwner** | <code>string</code> | <span></span>
**serviceStage** | <code>string</code> | <span></span>
**createNamedResources**? | <code>boolean</code> | __*Optional*__

### Methods


#### protected createAlarmName(alarmId) <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase-createalarmname"></a>

Create a CloudWatch alarm name.

```ts
protected createAlarmName(alarmId: string): string
```

* **alarmId** (<code>string</code>)  *No description*

__Returns__:
* <code>string</code>

#### protected createFullName(id) <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase-createfullname"></a>



```ts
protected createFullName(id: string): string
```

* **id** (<code>string</code>)  *No description*

__Returns__:
* <code>string</code>

#### protected createLogGroupName(name) <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase-createloggroupname"></a>

Create a custom CloudWatch log group name.

```ts
protected createLogGroupName(name: string): string
```

* **name** (<code>string</code>)  *No description*

__Returns__:
* <code>string</code>

#### protected createName(id, delimiter, props?) <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase-createname"></a>



```ts
protected createName(id: string, delimiter: string, props?: CreateNameProps): string
```

* **id** (<code>string</code>)  *No description*
* **delimiter** (<code>string</code>)  *No description*
* **props** (<code>[CreateNameProps](#softcomweb-ec-cdkpatterns-microservice-createnameprops)</code>)  *No description*
  * **includeServiceIdentity** (<code>boolean</code>)  *No description* __*Optional*__

__Returns__:
* <code>string</code>

#### protected createParameterName(name) <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase-createparametername"></a>

Create an SSM compatible parameter name.

```ts
protected createParameterName(name: string): string
```

* **name** (<code>string</code>)  *No description*

__Returns__:
* <code>string</code>

#### protected createRegularNameWithoutIdentity(id) <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase-createregularnamewithoutidentity"></a>



```ts
protected createRegularNameWithoutIdentity(id: string): string
```

* **id** (<code>string</code>)  *No description*

__Returns__:
* <code>string</code>

#### protected createSanitizedName(name, delimiter?) <a id="softcomweb-ec-cdkpatterns-microservice-microservicebase-createsanitizedname"></a>

```ts
protected createSanitizedName(name: string, delimiter?: string): string
```

* **name** (<code>string</code>)  the name of the result.
* **delimiter** (<code>string</code>)  the delimiter to use for joining the parts.

__Returns__:
* <code>string</code>

#### protected onPrepare() no longer available

The <code>onPrepare()</code> hook was used with CDKv1.x to perform final modifications before
synthesis, if necessary.

With CDKv2.x, using `constructs` instead of `@aws-cdk/core`, this feature has been
removed. As described in the [RFC](https://github.com/aws/aws-cdk-rfcs/blob/master/text/0192-remove-constructs-compat.md#06-no-prepare-the-prepare-hook-is-no-longer-supported),
one should use other features like e.g. Lazy to perform these kinds of modifications.

See the RFC for an example of how to emulate the previous behaviour, if
really necessary.

## struct CreateNameProps  <a id="softcomweb-ec-cdkpatterns-microservice-createnameprops"></a>

Name | Type | Description
-----|------|-------------
**includeServiceIdentity**? | <code>boolean</code> | __*Optional*__

## struct LambdaEventedMicroserviceProps  <a id="softcomweb-ec-cdkpatterns-microservice-lambdaeventedmicroserviceprops"></a>

Name | Type | Description
-----|------|-------------
**eventPattern** | <code>[EventPattern](#aws-cdk-aws-events-eventpattern)</code> | <span></span>
**handler** | <code>string</code> | FQDN of the Lambda handler.
**handlerRuntime** | <code>[Runtime](#aws-cdk-aws-lambda-runtime)</code> | Runtime of the handler.
**createNamedResources**? | <code>boolean</code> | __*Optional*__
**eventBus**? | <code>[IEventBus](#aws-cdk-aws-events-ieventbus)</code> | __*Optional*__
**eventRuleEnabled**? | <code>boolean</code> | __*Optional*__
**existingFunction**? | <code>[Function](#aws-cdk-aws-lambda-function)</code> | Use an existing Lambda function instead of creating a new one.<br/>__*Optional*__
**handlerCodeAsset**? | <code>[Code](#aws-cdk-aws-lambda-code)</code> | Lambda code.<br/>__*Optional*__
**handlerDeploymentPostTrafficHook**? | <code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code> | Deployment hook before traffic is routed to the Lambda function.<br/>__*Default*__: undefined
**handlerDeploymentPreTrafficHook**? | <code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code> | Deployment hook after traffic is routed to the Lambda function.<br/>__*Default*__: undefined
**handlerDeploymentStrategy**? | <code>[ILambdaDeploymentConfig](#aws-cdk-aws-codedeploy-ilambdadeploymentconfig)</code> | Deployment strategy for CodeDeploy.<br/>__*Default*__: codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE
**handlerInitialPolicy**? | <code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code> | IAM policies for the handler.<br/>__*Default*__: undefined
**handlerInsightsVersion**? | <code>[LambdaInsightsVersion](#aws-cdk-aws-lambda-lambdainsightsversion)</code> | IAM policies for the handler.<br/>__*Default*__: undefined
**handlerLayers**? | <code>Array<[ILayerVersion](#aws-cdk-aws-lambda-ilayerversion)></code> | Lambda layers to add to the Lambda.<br/>__*Default*__: no layers are added
**handlerLogRetention**? | <code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code> | Log retention for Lambda logs.<br/>__*Default*__: 7 days
**handlerMemorySize**? | <code>number</code> | Memory size.<br/>__*Default*__: 128
**handlerTimeout**? | <code>[Duration](#aws-cdk-core-duration)</code> | Timeout of the handler.<br/>__*Default*__: 3 seconds
**handlerVpc**? | <code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code> | VPC to put the Lambda to.<br/>__*Default*__: undefined
**handlerVpcSecurityGroups**? | <code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code> | Security groups to attach to the Lambda function.<br/>__*Default*__: a new security group is created
**handlerVpcSubnets**? | <code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code> | Subnets to use when putting Lambda to the VPC.<br/>__*Default*__: all subnets
**pythonEntry**? | <code>string</code> | Root path of the Lambda handler code.<br/>__*Optional*__
**pythonIndex**? | <code>string</code> | Index file for Python lambdas.<br/>__*Default*__: handler.py
**serviceIdentity**? | <code>string</code> | __*Optional*__
**serviceName**? | <code>string</code> | __*Optional*__
**serviceOwner**? | <code>string</code> | __*Optional*__
**serviceStage**? | <code>string</code> | __*Optional*__

## struct LambdaMicroserviceProps  <a id="softcomweb-ec-cdkpatterns-microservice-lambdamicroserviceprops"></a>

Name | Type | Description
-----|------|-------------
**handler** | <code>string</code> | FQDN of the Lambda handler.
**handlerRuntime** | <code>[Runtime](#aws-cdk-aws-lambda-runtime)</code> | Runtime of the handler.
**createNamedResources**? | <code>boolean</code> | __*Optional*__
**existingFunction**? | <code>[Function](#aws-cdk-aws-lambda-function)</code> | Use an existing Lambda function instead of creating a new one.<br/>__*Optional*__
**handlerCodeAsset**? | <code>[Code](#aws-cdk-aws-lambda-code)</code> | Lambda code.<br/>__*Optional*__
**handlerDeploymentPostTrafficHook**? | <code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code> | Deployment hook before traffic is routed to the Lambda function.<br/>__*Default*__: undefined
**handlerDeploymentPreTrafficHook**? | <code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code> | Deployment hook after traffic is routed to the Lambda function.<br/>__*Default*__: undefined
**handlerDeploymentStrategy**? | <code>[ILambdaDeploymentConfig](#aws-cdk-aws-codedeploy-ilambdadeploymentconfig)</code> | Deployment strategy for CodeDeploy.<br/>__*Default*__: codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE
**handlerInitialPolicy**? | <code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code> | IAM policies for the handler.<br/>__*Default*__: undefined
**handlerInsightsVersion**? | <code>[LambdaInsightsVersion](#aws-cdk-aws-lambda-lambdainsightsversion)</code> | IAM policies for the handler.<br/>__*Default*__: undefined
**handlerLayers**? | <code>Array<[ILayerVersion](#aws-cdk-aws-lambda-ilayerversion)></code> | Lambda layers to add to the Lambda.<br/>__*Default*__: no layers are added
**handlerLogRetention**? | <code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code> | Log retention for Lambda logs.<br/>__*Default*__: 7 days
**handlerMemorySize**? | <code>number</code> | Memory size.<br/>__*Default*__: 128
**handlerTimeout**? | <code>[Duration](#aws-cdk-core-duration)</code> | Timeout of the handler.<br/>__*Default*__: 3 seconds
**handlerVpc**? | <code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code> | VPC to put the Lambda to.<br/>__*Default*__: undefined
**handlerVpcSecurityGroups**? | <code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code> | Security groups to attach to the Lambda function.<br/>__*Default*__: a new security group is created
**handlerVpcSubnets**? | <code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code> | Subnets to use when putting Lambda to the VPC.<br/>__*Default*__: all subnets
**pythonEntry**? | <code>string</code> | Root path of the Lambda handler code.<br/>__*Optional*__
**pythonIndex**? | <code>string</code> | Index file for Python lambdas.<br/>__*Default*__: handler.py
**serviceIdentity**? | <code>string</code> | __*Optional*__
**serviceName**? | <code>string</code> | __*Optional*__
**serviceOwner**? | <code>string</code> | __*Optional*__
**serviceStage**? | <code>string</code> | __*Optional*__

## struct LambdaScheduledMicroserviceProps  <a id="softcomweb-ec-cdkpatterns-microservice-lambdascheduledmicroserviceprops"></a>

Name | Type | Description
-----|------|-------------
**eventSchedule** | <code>[Schedule](#aws-cdk-aws-events-schedule)</code> | <span></span>
**handler** | <code>string</code> | FQDN of the Lambda handler.
**handlerRuntime** | <code>[Runtime](#aws-cdk-aws-lambda-runtime)</code> | Runtime of the handler.
**createNamedResources**? | <code>boolean</code> | __*Optional*__
**eventBus**? | <code>[IEventBus](#aws-cdk-aws-events-ieventbus)</code> | __*Optional*__
**eventRuleEnabled**? | <code>boolean</code> | __*Optional*__
**existingFunction**? | <code>[Function](#aws-cdk-aws-lambda-function)</code> | Use an existing Lambda function instead of creating a new one.<br/>__*Optional*__
**handlerCodeAsset**? | <code>[Code](#aws-cdk-aws-lambda-code)</code> | Lambda code.<br/>__*Optional*__
**handlerDeploymentPostTrafficHook**? | <code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code> | Deployment hook before traffic is routed to the Lambda function.<br/>__*Default*__: undefined
**handlerDeploymentPreTrafficHook**? | <code>[IFunction](#aws-cdk-aws-lambda-ifunction)</code> | Deployment hook after traffic is routed to the Lambda function.<br/>__*Default*__: undefined
**handlerDeploymentStrategy**? | <code>[ILambdaDeploymentConfig](#aws-cdk-aws-codedeploy-ilambdadeploymentconfig)</code> | Deployment strategy for CodeDeploy.<br/>__*Default*__: codedeploy.LambdaDeploymentConfig.ALL_AT_ONCE
**handlerInitialPolicy**? | <code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code> | IAM policies for the handler.<br/>__*Default*__: undefined
**handlerInsightsVersion**? | <code>[LambdaInsightsVersion](#aws-cdk-aws-lambda-lambdainsightsversion)</code> | IAM policies for the handler.<br/>__*Default*__: undefined
**handlerLayers**? | <code>Array<[ILayerVersion](#aws-cdk-aws-lambda-ilayerversion)></code> | Lambda layers to add to the Lambda.<br/>__*Default*__: no layers are added
**handlerLogRetention**? | <code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code> | Log retention for Lambda logs.<br/>__*Default*__: 7 days
**handlerMemorySize**? | <code>number</code> | Memory size.<br/>__*Default*__: 128
**handlerTimeout**? | <code>[Duration](#aws-cdk-core-duration)</code> | Timeout of the handler.<br/>__*Default*__: 3 seconds
**handlerVpc**? | <code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code> | VPC to put the Lambda to.<br/>__*Default*__: undefined
**handlerVpcSecurityGroups**? | <code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code> | Security groups to attach to the Lambda function.<br/>__*Default*__: a new security group is created
**handlerVpcSubnets**? | <code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code> | Subnets to use when putting Lambda to the VPC.<br/>__*Default*__: all subnets
**pythonEntry**? | <code>string</code> | Root path of the Lambda handler code.<br/>__*Optional*__
**pythonIndex**? | <code>string</code> | Index file for Python lambdas.<br/>__*Default*__: handler.py
**serviceIdentity**? | <code>string</code> | __*Optional*__
**serviceName**? | <code>string</code> | __*Optional*__
**serviceOwner**? | <code>string</code> | __*Optional*__
**serviceStage**? | <code>string</code> | __*Optional*__

## struct MicroserviceBaseProps  <a id="softcomweb-ec-cdkpatterns-microservice-microservicebaseprops"></a>

Name | Type | Description
-----|------|-------------
**createNamedResources**? | <code>boolean</code> | __*Optional*__
**serviceIdentity**? | <code>string</code> | __*Optional*__
**serviceName**? | <code>string</code> | __*Optional*__
**serviceOwner**? | <code>string</code> | __*Optional*__
**serviceStage**? | <code>string</code> | __*Optional*__
