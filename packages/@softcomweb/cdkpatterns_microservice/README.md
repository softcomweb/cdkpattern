# @softcomweb-ec/cdkpatterns_microservice

<!--BEGIN STABILITY BANNER-->

---

![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> Standardized setup for microservices (Lambda).

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/cdkpatterns_microservice
```

## How to Use

```js
import * as cdk from "aws-cdk-lib";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as cdkpatterns_microservice from "@softcomweb-ec/cdkpatterns_microservice";

new cdkpatterns_microservice.LambdaMicroservice(this, "ms", {
    serviceIdentity: "myservice", // optional. can be set through the feature flag @softcomweb-ec/cdkpatterns_microservice:serviceIdentity globally for all resources within the app
    serviceName: "backend",
    serviceStage: "dev", // optional. can be set through the feature flag @softcomweb-ec/cdkpatterns_microservice:serviceStage globally for all resources within the app
    serviceOwner: "myteam@softcomweb.de" // optional. can be set through the feature flag @softcomweb-ec/cdkpatterns_microservice:serviceOwner globally for all resources within the app

    handler: "handler.handle_request",
    handlerCodeAsset: lambda.Code.fromAsset(`${__dirname}/lambda.d`),
    handlerRuntime: lambda.Runtime.PYTHON_3_9,
});
```

### Python Support

If you are using the Python runtime, you can let CDK auto-install dependencies. See the [@aws-cdk/aws-lambda-python-alpha](https://github.com/aws/aws-cdk/tree/master/packages/%40aws-cdk/aws-lambda-python) docs.

```js
import * as cdk from "aws-cdk-lib";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as cdkpatterns_microservice from "@softcomweb-ec/cdkpatterns_microservice";

new cdkpatterns_microservice.LambdaMicroservice(this, "ms", {
    serviceIdentity: "myservice",
    serviceName: "backend",
    serviceStage: "dev",

    handler: "handler.handle_request",
    handlerRuntime: lambda.Runtime.PYTHON_3_9,

    // Enable Python support
    pythonEntry: `${__dirname}/lambda.d/python`,
    pythonIndex: "handler.py",
});
```

## API Reference

See [API.md](API.md).
