# API Reference

**Classes**

Name|Description
----|-----------
[Pipeline](#softcomweb-ec-cdkpatterns-pipelines-pipeline)|*No description*


**Structs**

Name|Description
----|-----------
[PipelineProps](#softcomweb-ec-cdkpatterns-pipelines-pipelineprops)|*No description*



## class Pipeline  <a id="softcomweb-ec-cdkpatterns-pipelines-pipeline"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable)
__Extends__: [Construct](#aws-cdk-core-construct)

### Initializer




```ts
new Pipeline(scope: Construct, id: string, props: PipelineProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[PipelineProps](#softcomweb-ec-cdkpatterns-pipelines-pipelineprops)</code>)  *No description*
  * **githubConnectionArn** (<code>string</code>)  *No description* 
  * **githubRepositoryName** (<code>string</code>)  *No description* 
  * **githubRepositoryOwner** (<code>string</code>)  *No description* 
  * **buildComputeType** (<code>[ComputeType](#aws-cdk-aws-codebuild-computetype)</code>)  *No description* __*Optional*__
  * **buildEnvironmentVariables** (<code>Map<string, [BuildEnvironmentVariable](#aws-cdk-aws-codebuild-buildenvironmentvariable)></code>)  *No description* __*Optional*__
  * **buildHasDockerEnabled** (<code>boolean</code>)  *No description* __*Optional*__
  * **buildImage** (<code>[LinuxBuildImage](#aws-cdk-aws-codebuild-linuxbuildimage)</code>)  *No description* __*Optional*__
  * **buildPartialBuildSpec** (<code>[BuildSpec](#aws-cdk-aws-codebuild-buildspec)</code>)  *No description* __*Optional*__
  * **buildRolePolicy** (<code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code>)  *No description* __*Optional*__
  * **buildVpc** (<code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code>)  *No description* __*Optional*__
  * **buildVpcSecurityGroups** (<code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code>)  *No description* __*Optional*__
  * **buildVpcSubnets** (<code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code>)  *No description* __*Optional*__
  * **githubConnectionTriggerOnPush** (<code>boolean</code>)  *No description* __*Optional*__
  * **githubRepositoryBranch** (<code>string</code>)  *No description* __*Optional*__
  * **pipelineCrossAccountKeysEnabled** (<code>boolean</code>)  *No description* __*Optional*__
  * **pipelineMsTeamsWebhookEvents** (<code>Array<[PipelineEvent](#cloudcomponents-cdk-developer-tools-notifications-pipelineevent)></code>)  *No description* __*Optional*__
  * **pipelineMsTeamsWebhookUrl** (<code>string</code>)  *No description* __*Optional*__
  * **pipelineName** (<code>string</code>)  *No description* __*Optional*__
  * **pipelineSchedules** (<code>Map<string, [Schedule](#aws-cdk-aws-events-schedule)></code>)  *No description* __*Optional*__
  * **pipelineSelfMutationEnabled** (<code>boolean</code>)  *No description* __*Optional*__
  * **pipelineSource** (<code>[CodePipelineSource](#aws-cdk-pipelines-codepipelinesource)</code>)  *No description* __*Optional*__
  * **preSubmitBuildSpec** (<code>[BuildSpec](#aws-cdk-aws-codebuild-buildspec)</code>)  *No description* __*Optional*__
  * **preSubmitEnabled** (<code>boolean</code>)  *No description* __*Optional*__
  * **synthCommands** (<code>Array<string></code>)  *No description* __*Optional*__
  * **synthPrimaryOutputDirectory** (<code>string</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description 
-----|------|-------------
**buildLogs** | <code>[LogGroup](#aws-cdk-aws-logs-loggroup)</code> | <span></span>
**codePipeline** | <code>[CodePipeline](#aws-cdk-pipelines-codepipeline)</code> | <span></span>

### Methods


#### addStage(stage, options?) <a id="softcomweb-ec-cdkpatterns-pipelines-pipeline-addstage"></a>



```ts
addStage(stage: Stage, options?: AddStageOpts): StageDeployment
```

* **stage** (<code>[Stage](#aws-cdk-core-stage)</code>)  *No description*
* **options** (<code>[AddStageOpts](#aws-cdk-pipelines-addstageopts)</code>)  *No description*
  * **post** (<code>Array<[Step](#aws-cdk-pipelines-step)></code>)  Additional steps to run after all of the stacks in the stage. __*Default*__: No additional steps
  * **pre** (<code>Array<[Step](#aws-cdk-pipelines-step)></code>)  Additional steps to run before any of the stacks in the stage. __*Default*__: No additional steps
  * **stackSteps** (<code>Array<[StackSteps](#aws-cdk-pipelines-stacksteps)></code>)  Instructions for stack level steps. __*Default*__: No additional instructions

__Returns__:
* <code>[StageDeployment](#aws-cdk-pipelines-stagedeployment)</code>

#### addWave(id, opts) <a id="softcomweb-ec-cdkpatterns-pipelines-pipeline-addwave"></a>



```ts
addWave(id: string, opts: WaveOptions): Wave
```

* **id** (<code>string</code>)  *No description*
* **opts** (<code>[WaveOptions](#aws-cdk-pipelines-waveoptions)</code>)  *No description*
  * **post** (<code>Array<[Step](#aws-cdk-pipelines-step)></code>)  Additional steps to run after all of the stages in the wave. __*Default*__: No additional steps
  * **pre** (<code>Array<[Step](#aws-cdk-pipelines-step)></code>)  Additional steps to run before any of the stages in the wave. __*Default*__: No additional steps

__Returns__:
* <code>[Wave](#aws-cdk-pipelines-wave)</code>

#### protected onSynthesize(session) <a id="softcomweb-ec-cdkpatterns-pipelines-pipeline-onsynthesize"></a>

Allows this construct to emit artifacts into the cloud assembly during synthesis.

This method is usually implemented by framework-level constructs such as `Stack` and `Asset`
as they participate in synthesizing the cloud assembly.

```ts
protected onSynthesize(session: ISynthesisSession): void
```

* **session** (<code>[ISynthesisSession](#constructs-isynthesissession)</code>)  *No description*






## struct PipelineProps  <a id="softcomweb-ec-cdkpatterns-pipelines-pipelineprops"></a>






Name | Type | Description 
-----|------|-------------
**githubConnectionArn** | <code>string</code> | <span></span>
**githubRepositoryName** | <code>string</code> | <span></span>
**githubRepositoryOwner** | <code>string</code> | <span></span>
**buildComputeType**? | <code>[ComputeType](#aws-cdk-aws-codebuild-computetype)</code> | __*Optional*__
**buildEnvironmentVariables**? | <code>Map<string, [BuildEnvironmentVariable](#aws-cdk-aws-codebuild-buildenvironmentvariable)></code> | __*Optional*__
**buildHasDockerEnabled**? | <code>boolean</code> | __*Optional*__
**buildImage**? | <code>[LinuxBuildImage](#aws-cdk-aws-codebuild-linuxbuildimage)</code> | __*Optional*__
**buildPartialBuildSpec**? | <code>[BuildSpec](#aws-cdk-aws-codebuild-buildspec)</code> | __*Optional*__
**buildRolePolicy**? | <code>Array<[PolicyStatement](#aws-cdk-aws-iam-policystatement)></code> | __*Optional*__
**buildVpc**? | <code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code> | __*Optional*__
**buildVpcSecurityGroups**? | <code>Array<[ISecurityGroup](#aws-cdk-aws-ec2-isecuritygroup)></code> | __*Optional*__
**buildVpcSubnets**? | <code>[SubnetSelection](#aws-cdk-aws-ec2-subnetselection)</code> | __*Optional*__
**githubConnectionTriggerOnPush**? | <code>boolean</code> | __*Optional*__
**githubRepositoryBranch**? | <code>string</code> | __*Optional*__
**pipelineCrossAccountKeysEnabled**? | <code>boolean</code> | __*Optional*__
**pipelineMsTeamsWebhookEvents**? | <code>Array<[PipelineEvent](#cloudcomponents-cdk-developer-tools-notifications-pipelineevent)></code> | __*Optional*__
**pipelineMsTeamsWebhookUrl**? | <code>string</code> | __*Optional*__
**pipelineName**? | <code>string</code> | __*Optional*__
**pipelineSchedules**? | <code>Map<string, [Schedule](#aws-cdk-aws-events-schedule)></code> | __*Optional*__
**pipelineSelfMutationEnabled**? | <code>boolean</code> | __*Optional*__
**pipelineSource**? | <code>[CodePipelineSource](#aws-cdk-pipelines-codepipelinesource)</code> | __*Optional*__
**preSubmitBuildSpec**? | <code>[BuildSpec](#aws-cdk-aws-codebuild-buildspec)</code> | __*Optional*__
**preSubmitEnabled**? | <code>boolean</code> | __*Optional*__
**synthCommands**? | <code>Array<string></code> | __*Optional*__
**synthPrimaryOutputDirectory**? | <code>string</code> | __*Optional*__



