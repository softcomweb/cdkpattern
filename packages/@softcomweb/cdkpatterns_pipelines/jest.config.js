module.exports = {
    roots: ["<rootDir>/src"],
    testMatch: ["**/*.test.ts"],
    transform: {
        "^.+\\.tsx?$": "ts-jest",
    },
    collectCoverageFrom: ["src/*.ts"],
};
