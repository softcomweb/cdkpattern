# @softcomweb-ec/cdkpatterns_pipelines

<!--BEGIN STABILITY BANNER-->

---
![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> CI/CD pipelines for CDK with @aws-cdk/pipelines wrapper.

This module wraps [@aws-cdk/pipelines](https://github.com/aws/aws-cdk/tree/master/packages/%40aws-cdk/pipelines) with additional goodies, like

- scheduled pipeline executions to keep your deployments fresh (optional)
- notifications about pipeline events via MSTeams webhooks (optional)
- sane defaults for CDK apps
- removing `DeletionPolicy: Retain` from resources
- self-updating pipelines (optional)

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/si_cdkpatterns_starter
```

## How to Use

```js
import * as cdk from "@aws-cdk/core";
import * as cdkpatterns_pipelines from "@softcomweb-ec/cdkpatterns_pipelines";

class MyDeploymentStage extends cdk.Stage {
    constructor(scope: cdk.Construct, id: string, props: cdk.StageProps) {
        super(scope, id, props);

        // create an instance of some construct deriving from cdk.Stack 
        new cdk.Stack(this, "mystack", {});
    }
}

const pipeline = new cdkpatterns_pipelines.Pipeline(this, "pipeline", {
    // CodeStar connection (must be created through the console)
    githubConnectionArn: "some-arn",
    githubRepositoryName: "myrepo",
    githubRepositoryOwner: "softcomweb-ec",

    // optional override if not using the top-level directory
    // or any scripts
    synthCommands: [
        "cd cdk",
        "npm install",
        "./ci/myscript.sh",
        "npm run synth",
    ],
    synthPrimaryOutputDirectory: "cdk/cdk.out",
});

// deploy to a stage
pipeline.addStage(new MyDeploymentStage(this, "beta", {
    // this is required as pipelines needs to know where to deploy to
    // process.env.CDK_DEFAULT_REGION and process.env.CDK_DEFAULT_ACCOUNT
    // cannot be used!
    env: {
        account: "111122223333",
        region: "eu-west-1"
    },
}));
```

## API Reference

See [API.md](API.md).