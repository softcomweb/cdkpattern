import * as codebuild from "@aws-cdk/aws-codebuild";
import * as cdk from "@aws-cdk/core";
import * as events from "@aws-cdk/aws-events";
import * as eventstargets from "@aws-cdk/aws-events-targets";
import * as s3 from "@aws-cdk/aws-s3";
import * as iam from "@aws-cdk/aws-iam";
import * as ec2 from "@aws-cdk/aws-ec2";
import * as kms from "@aws-cdk/aws-kms";
import * as logs from "@aws-cdk/aws-logs";
import * as pipelines from "@aws-cdk/pipelines";
import * as cdkchatops from "@cloudcomponents/cdk-chatops";
import * as cdkdevelopertoolsnotifications from "@cloudcomponents/cdk-developer-tools-notifications";
import * as constructs from "constructs";

export interface PipelineProps {
  readonly pipelineName?: string;
  readonly pipelineSelfMutationEnabled?: boolean;
  readonly pipelineCrossAccountKeysEnabled?: boolean;
  readonly pipelineMsTeamsWebhookUrl?: string;
  readonly pipelineMsTeamsWebhookEvents?: cdkdevelopertoolsnotifications.PipelineEvent[];
  readonly pipelineSource?: pipelines.CodePipelineSource;
  readonly pipelineSchedules?: Record<string, events.Schedule>;

  readonly githubRepositoryOwner: string;
  readonly githubRepositoryName: string;
  readonly githubRepositoryBranch?: string;
  readonly githubConnectionArn: string;
  readonly githubConnectionTriggerOnPush?: boolean;

  readonly synthCommands?: string[];
  readonly synthPrimaryOutputDirectory?: string;

  readonly buildEnvironmentVariables?: Record<
    string,
    codebuild.BuildEnvironmentVariable
  >;
  readonly buildImage?: codebuild.LinuxBuildImage;
  readonly buildComputeType?: codebuild.ComputeType;
  readonly buildPartialBuildSpec?: codebuild.BuildSpec;
  readonly buildHasDockerEnabled?: boolean;
  readonly buildRolePolicy?: iam.PolicyStatement[];
  readonly buildVpc?: ec2.IVpc;
  readonly buildVpcSubnets?: ec2.SubnetSelection;
  readonly buildVpcSecurityGroups?: ec2.ISecurityGroup[];

  readonly preSubmitEnabled?: boolean;
  readonly preSubmitBuildSpec?: codebuild.BuildSpec;
}

export class Pipeline extends cdk.Construct {
  public readonly codePipeline: pipelines.CodePipeline;
  public readonly buildLogs: logs.LogGroup;

  constructor(scope: cdk.Construct, id: string, props: PipelineProps) {
    super(scope, id);

    const pipelineBranch = props.githubRepositoryBranch || "main";
    const pipelineSourceRepo = `${props.githubRepositoryOwner}/${props.githubRepositoryName}`;

    const pipelineSource =
      props.pipelineSource ||
      pipelines.CodePipelineSource.connection(
        pipelineSourceRepo,
        pipelineBranch,
        {
          connectionArn: props.githubConnectionArn,
          triggerOnPush: props.githubConnectionTriggerOnPush,
        }
      );

    this.codePipeline = new pipelines.CodePipeline(this, "pipeline", {
      synth: new pipelines.ShellStep("synth", {
        commands: props.synthCommands || [
          "yarn install --frozen-lockfile",
          "$(npm bin)/cdk synth",
        ],
        input: pipelineSource,
        primaryOutputDirectory: props.synthPrimaryOutputDirectory || "cdk.out",
      }),
      crossAccountKeys: props.pipelineCrossAccountKeysEnabled || true,
      dockerEnabledForSelfMutation: props.buildHasDockerEnabled || true,
      dockerEnabledForSynth: props.buildHasDockerEnabled || true,
      pipelineName: props.pipelineName,
      selfMutation: props.pipelineSelfMutationEnabled,
      publishAssetsInParallel: true,
      codeBuildDefaults: {
        buildEnvironment: {
          buildImage:
            props.buildImage || codebuild.LinuxBuildImage.STANDARD_5_0,
          computeType: props.buildComputeType || codebuild.ComputeType.MEDIUM,
          privileged: true,
          environmentVariables: {
            ...props.buildEnvironmentVariables,
          },
        },
        partialBuildSpec: props.buildPartialBuildSpec,
        vpc: props.buildVpc,
        subnetSelection: props.buildVpcSubnets,
        securityGroups: props.buildVpcSecurityGroups,
        rolePolicy: props.buildRolePolicy,
      },
    });

    if (props.pipelineMsTeamsWebhookUrl) {
      new cdkdevelopertoolsnotifications.PipelineNotificationRule(
        this,
        "chatopsPipelineNotification",
        {
          events: props.pipelineMsTeamsWebhookEvents || [
            cdkdevelopertoolsnotifications.PipelineEvent
              .PIPELINE_EXECUTION_STARTED,
            cdkdevelopertoolsnotifications.PipelineEvent
              .PIPELINE_EXECUTION_FAILED,
            cdkdevelopertoolsnotifications.PipelineEvent
              .PIPELINE_EXECUTION_SUCCEEDED,
          ],
          name: `${props.githubRepositoryOwner}-${props.githubRepositoryName}-notification`,
          pipeline: this.codePipeline.pipeline,
          targets: [
            new cdkdevelopertoolsnotifications.MSTeamsIncomingWebhook(
              new cdkchatops.MSTeamsIncomingWebhookConfiguration(
                this,
                "chatopsWebhook",
                {
                  url: props.pipelineMsTeamsWebhookUrl,
                  accountLabelMode: cdkchatops.AccountLabelMode.ID_AND_ALIAS,
                  themeColor: "#FF0000",
                }
              )
            ),
          ],
        }
      );
    }

    this.buildLogs = new logs.LogGroup(this, "buildLogs", {
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      retention: logs.RetentionDays.ONE_WEEK,
    });

    if (props.preSubmitEnabled) {
      new codebuild.Project(this, "presubmitTrigger", {
        buildSpec:
          props.preSubmitBuildSpec ||
          codebuild.BuildSpec.fromSourceFilename("buildspec.yml"),
        environment: {
          buildImage: props.buildImage,
          computeType: props.buildComputeType,
          environmentVariables: {
            ...props.buildEnvironmentVariables,
          },
          privileged: props.buildHasDockerEnabled || true,
        },
        grantReportGroupPermissions: true,
        checkSecretsInPlainTextEnvVariables: true,
        description: `PR builds for https://github.com/${pipelineSourceRepo}`,
        source: codebuild.Source.gitHub({
          owner: props.githubRepositoryOwner,
          repo: props.githubRepositoryName,
          branchOrRef: pipelineBranch,
          reportBuildStatus: true,
          fetchSubmodules: true,
          webhookFilters: [
            codebuild.FilterGroup.inEventOf(
              codebuild.EventAction.PULL_REQUEST_CREATED,
              codebuild.EventAction.PULL_REQUEST_REOPENED,
              codebuild.EventAction.PULL_REQUEST_UPDATED
            )
              .andBaseBranchIs(pipelineBranch)
              .andFilePathIs(".*"),
          ],
        }),
        vpc: props.buildVpc,
        subnetSelection: props.buildVpcSubnets,
        securityGroups: props.buildVpcSecurityGroups,
      });

      Object.keys(props.pipelineSchedules || {}).forEach((triggerId) => {
        new events.Rule(this, "pipelinePeriodicTrigger" + triggerId, {
          description: `triggers ${this.codePipeline.pipeline.pipelineName} periodically`,
          enabled: true,
          schedule: props.pipelineSchedules![triggerId],
          targets: [
            new eventstargets.CodePipeline(this.codePipeline.pipeline, {
              retryAttempts: 2,
            }),
          ],
        });
      });
    }
  }

  public addStage(
    stage: cdk.Stage,
    options?: pipelines.AddStageOpts
  ): pipelines.StageDeployment {
    return this.codePipeline.addStage(stage, options);
  }

  public addWave(id: string, opts: pipelines.WaveOptions): pipelines.Wave {
    return this.codePipeline.addWave(id, opts);
  }

  protected onSynthesize(session: constructs.ISynthesisSession) {
    session; // we do not need this parameter, however JSII and TSC otherwise complain about unused variables

    this.node.findAll().forEach((node) => {
      if (node instanceof s3.CfnBucket) {
        (node as s3.CfnBucket).applyRemovalPolicy(cdk.RemovalPolicy.DESTROY);
        (node as s3.CfnBucket).lifecycleConfiguration = {
          rules: [
            {
              abortIncompleteMultipartUpload: {
                daysAfterInitiation: 1,
              },
              expirationInDays: 30,
              noncurrentVersionExpirationInDays: 30,
              status: "Enabled",
            },
          ],
        };
      }
      if (node instanceof logs.CfnLogGroup) {
        (node as logs.CfnLogGroup).applyRemovalPolicy(
          cdk.RemovalPolicy.DESTROY
        );
      }
      if (node instanceof kms.CfnKey) {
        (node as kms.CfnKey).applyRemovalPolicy(cdk.RemovalPolicy.DESTROY);
        (node as kms.CfnKey).enableKeyRotation = true;
        (
          node as kms.CfnKey
        ).description = `encrypts artifacts of ${this.codePipeline.pipeline.pipelineArn}`;
      }
      if (node instanceof codebuild.CfnProject) {
        if ((node as codebuild.CfnProject).description === undefined) {
          (
            node as codebuild.CfnProject
          ).description = `Builds for ${this.codePipeline.pipeline.pipelineArn}`;
        }
        (node as codebuild.CfnProject).logsConfig = {
          cloudWatchLogs: {
            status: "ENABLED",
            groupName: this.buildLogs.logGroupName,
          },
        };
      }
    });
  }
}
