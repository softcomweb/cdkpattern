# API Reference

**Classes**

Name|Description
----|-----------
[Network](#softcomweb-ec-cdkpatterns-network-network)|*No description*


**Structs**

Name|Description
----|-----------
[NetworkProps](#softcomweb-ec-cdkpatterns-network-networkprops)|*No description*



## class Network  <a id="softcomweb-ec-cdkpatterns-network-network"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable)
__Extends__: [Construct](#aws-cdk-core-construct)

### Initializer




```ts
new Network(scope: Construct, id: string, props: NetworkProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[NetworkProps](#softcomweb-ec-cdkpatterns-network-networkprops)</code>)  *No description*
  * **flowLogOptions** (<code>[FlowLogOptions](#aws-cdk-aws-ec2-flowlogoptions)</code>)  *No description* __*Optional*__
  * **flowLogRetention** (<code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code>)  *No description* __*Optional*__
  * **vpcMaxAzs** (<code>number</code>)  Number of AZs to use. __*Default*__: 2
  * **vpcNatGateways** (<code>number</code>)  Number of NAT gateways to use. __*Default*__: 2



### Properties


Name | Type | Description 
-----|------|-------------
**vpc** | <code>[IVpc](#aws-cdk-aws-ec2-ivpc)</code> | <span></span>
**vpcEgressIps** | <code>Array<string></code> | <span></span>
**vpcFlowLog** | <code>[IFlowLog](#aws-cdk-aws-ec2-iflowlog)</code> | <span></span>
**vpcFlowLogGroup** | <code>[LogGroup](#aws-cdk-aws-logs-loggroup)</code> | <span></span>

### Methods


#### addVpcEndpoints() <a id="softcomweb-ec-cdkpatterns-network-network-addvpcendpoints"></a>



```ts
addVpcEndpoints(): void
```







## struct NetworkProps  <a id="softcomweb-ec-cdkpatterns-network-networkprops"></a>






Name | Type | Description 
-----|------|-------------
**flowLogOptions**? | <code>[FlowLogOptions](#aws-cdk-aws-ec2-flowlogoptions)</code> | __*Optional*__
**flowLogRetention**? | <code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code> | __*Optional*__
**vpcMaxAzs**? | <code>number</code> | Number of AZs to use.<br/>__*Default*__: 2
**vpcNatGateways**? | <code>number</code> | Number of NAT gateways to use.<br/>__*Default*__: 2



