# @softcomweb-ec/cdkpatterns_network

<!--BEGIN STABILITY BANNER-->

---

![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> An easier to use VPC setup with standard softcomweb settings.

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/si_cdkpatterns_network
```

## How to Use

```js
import * as cdk from "@aws-cdk/core";
import * as cdkpatterns_network from "@softcomweb-ec/cdkpatterns_network";

new cdkpatterns_network.Network(this, "network", {

});
```

## API Reference

See [API.md](API.md).