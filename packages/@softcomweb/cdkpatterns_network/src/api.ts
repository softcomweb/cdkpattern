import * as ec2 from "@aws-cdk/aws-ec2";
import * as logs from "@aws-cdk/aws-logs";
// import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as cdk from "@aws-cdk/core";

const VPC_MAX_AZS_DEFAULT = 2;
const VPC_NAT_GATEWAYS_DEFAULT = 2;
const VPC_FLOWLOGS_RETENTION_DEFAULT = logs.RetentionDays.ONE_WEEK;
const VPC_FLOWLOGS_TRAFFICTYPE_DEFAULT = ec2.FlowLogTrafficType.ALL;

export interface NetworkProps {
  /**
   * Number of AZs to use.
   *
   * @default 2
   */
  readonly vpcMaxAzs?: number;
  /**
   * Number of NAT gateways to use
   *
   * @default 2
   */
  readonly vpcNatGateways?: number;
  readonly flowLogOptions?: ec2.FlowLogOptions;
  readonly flowLogRetention?: logs.RetentionDays;
}

export class Network extends cdk.Construct {
  public readonly vpc: ec2.IVpc;
  public readonly vpcEgressIps: string[];
  public readonly vpcFlowLog: ec2.IFlowLog;
  public readonly vpcFlowLogGroup: logs.LogGroup;

  constructor(scope: cdk.Construct, id: string, props: NetworkProps) {
    super(scope, id);

    this.vpc = new ec2.Vpc(this, "vpc", {
      enableDnsHostnames: true,
      enableDnsSupport: true,
      maxAzs: props.vpcMaxAzs || VPC_MAX_AZS_DEFAULT,
      natGateways: props.vpcNatGateways || VPC_NAT_GATEWAYS_DEFAULT,
      flowLogs: {},
      subnetConfiguration: [
        {
          cidrMask: 24,
          name: "Public",
          subnetType: ec2.SubnetType.PUBLIC,
        },
        {
          cidrMask: 22,
          name: "Private",
          subnetType: ec2.SubnetType.PRIVATE_WITH_NAT,
        },
        {
          cidrMask: 22,
          name: "Protected",
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
        },
      ],
    });

    this.vpcEgressIps = this.vpc.node
      .findAll()
      .filter((node) => node instanceof ec2.CfnEIP)
      .map((node) => (node as ec2.CfnEIP).attrAllocationId);

    // NACL for Public Subnets
    const naclPublic = new ec2.NetworkAcl(this, "naclPublic", {
      vpc: this.vpc,
      subnetSelection: {
        subnetType: ec2.SubnetType.PUBLIC,
      },
    });

    // Egress Rules for Public Subnets
    naclPublic.addEntry("naclEgressPublic", {
      direction: ec2.TrafficDirection.EGRESS,
      ruleNumber: 100,
      cidr: ec2.AclCidr.anyIpv4(),
      traffic: ec2.AclTraffic.allTraffic(),
      ruleAction: ec2.Action.ALLOW,
    });

    // Ingress Rules for Public Subnets
    naclPublic.addEntry("naclIngressPublic", {
      direction: ec2.TrafficDirection.INGRESS,
      ruleNumber: 100,
      cidr: ec2.AclCidr.anyIpv4(),
      traffic: ec2.AclTraffic.allTraffic(),
      ruleAction: ec2.Action.ALLOW,
    });

    // NACL for Private Subnets
    const naclPrivate = new ec2.NetworkAcl(this, "naclPrivate", {
      vpc: this.vpc,
      subnetSelection: {
        subnetType: ec2.SubnetType.PUBLIC,
      },
    });

    // Egress Rules for Private Subnets
    naclPrivate.addEntry("naclEgressPrivate", {
      direction: ec2.TrafficDirection.EGRESS,
      ruleNumber: 100,
      cidr: ec2.AclCidr.anyIpv4(),
      traffic: ec2.AclTraffic.allTraffic(),
      ruleAction: ec2.Action.ALLOW,
    });

    // Ingress Rules for Private Subnets
    naclPrivate.addEntry("naclIngressPrivate", {
      direction: ec2.TrafficDirection.INGRESS,
      ruleNumber: 100,
      cidr: ec2.AclCidr.anyIpv4(),
      traffic: ec2.AclTraffic.allTraffic(),
      ruleAction: ec2.Action.ALLOW,
    });

    this.vpcFlowLogGroup = new logs.LogGroup(this, "vpcFlowLogGroup", {
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      retention: props.flowLogRetention || VPC_FLOWLOGS_RETENTION_DEFAULT,
      logGroupName: `/aws/vpcflowlogs/${this.vpc.vpcId}`,
    });

    this.vpcFlowLog = this.vpc.addFlowLog("vpcFlowLogsToCloudWatch", {
      trafficType:
        props.flowLogOptions?.trafficType || VPC_FLOWLOGS_TRAFFICTYPE_DEFAULT,
      destination: ec2.FlowLogDestination.toCloudWatchLogs(
        this.vpcFlowLogGroup
      ),
    });

    // this.vpc.node.findAll().forEach((node, idx) => {
    //   if(node instanceof ec2.CfnNatGateway) {
    //     const natgw = node as ec2.CfnNatGateway;

    //     new cloudwatch.Alarm(this, `vpcNatGatewayPacketsDropCountAlarm${idx}`, {
    //       metric: new cloudwatch.Metric({
    //         metricName: "PacketsDropCount",
    //         namespace: "AWS/NATGateway",
    //         dimensionsMap: {
    //           NatGatewayId: natgw.ref,
    //         }
    //       }),
    //       evaluationPeriods: 1,
    //       threshold: 1,
    //     });

    //     new cloudwatch.Alarm(this, `vpcNatGatewayErrorPortAllocationAlarm${idx}`, {
    //       metric: new cloudwatch.Metric({
    //         metricName: "ErrorPortAllocation",
    //         namespace: "AWS/NATGateway",
    //         dimensionsMap: {
    //           NatGatewayId: natgw.ref,
    //         }
    //       }),
    //       evaluationPeriods: 1,
    //       threshold: 1,
    //     });
    //   }
    // });
  }

  public addVpcEndpoints() {
    const subnets = this.vpc.selectSubnets({
      subnetType: ec2.SubnetType.PRIVATE,
    });

    const vpcEndpointBaseOpts = {
      privateDnsEnabled: true,
      open: true,
      subnets,
    };

    this.vpc.addInterfaceEndpoint("SSM", {
      service: ec2.InterfaceVpcEndpointAwsService.SSM,
      ...vpcEndpointBaseOpts,
    });
    this.vpc.addInterfaceEndpoint("SSM_MESSAGES", {
      service: ec2.InterfaceVpcEndpointAwsService.SSM_MESSAGES,
      ...vpcEndpointBaseOpts,
    });
    this.vpc.addInterfaceEndpoint("CLOUDWATCH_LOGS", {
      service: ec2.InterfaceVpcEndpointAwsService.CLOUDWATCH_LOGS,
      ...vpcEndpointBaseOpts,
    });
    this.vpc.addInterfaceEndpoint("ECR_DOCKER", {
      service: ec2.InterfaceVpcEndpointAwsService.ECR_DOCKER,
      ...vpcEndpointBaseOpts,
    });
    this.vpc.addInterfaceEndpoint("EC2_MESSAGES", {
      service: ec2.InterfaceVpcEndpointAwsService.EC2_MESSAGES,
      ...vpcEndpointBaseOpts,
    });
    this.vpc.addInterfaceEndpoint("EC2", {
      service: ec2.InterfaceVpcEndpointAwsService.EC2,
      ...vpcEndpointBaseOpts,
    });
    this.vpc.addGatewayEndpoint("S3", {
      service: ec2.GatewayVpcEndpointAwsService.S3,
      subnets: [subnets],
    });
    this.vpc.addGatewayEndpoint("DYNAMODB", {
      service: ec2.GatewayVpcEndpointAwsService.DYNAMODB,
      subnets: [subnets],
    });
  }
}
