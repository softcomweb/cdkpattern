# API Reference

**Classes**

Name|Description
----|-----------
[WebCanary](#softcomweb-ec-cdkpatterns-webcanary-webcanary)|*No description*


**Structs**

Name|Description
----|-----------
[WebCanaryProps](#softcomweb-ec-cdkpatterns-webcanary-webcanaryprops)|*No description*



## class WebCanary  <a id="softcomweb-ec-cdkpatterns-webcanary-webcanary"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable)
__Extends__: [Construct](#aws-cdk-core-construct)

### Initializer




```ts
new WebCanary(scope: Construct, id: string, props: WebCanaryProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[WebCanaryProps](#softcomweb-ec-cdkpatterns-webcanary-webcanaryprops)</code>)  *No description*
  * **canaryBucket** (<code>[IBucket](#aws-cdk-aws-s3-ibucket)</code>)  *No description* 
  * **canaryRuntime** (<code>[Runtime](#aws-cdk-aws-synthetics-runtime)</code>)  *No description* 
  * **canaryTest** (<code>[Test](#aws-cdk-aws-synthetics-test)</code>)  *No description* 
  * **canaryBucketName** (<code>string</code>)  *No description* __*Optional*__
  * **canaryBucketPrefix** (<code>string</code>)  *No description* __*Optional*__
  * **canaryEnvironmentVariables** (<code>Map<string, string></code>)  *No description* __*Optional*__
  * **canaryMemory** (<code>number</code>)  *No description* __*Optional*__
  * **canaryName** (<code>string</code>)  *No description* __*Optional*__
  * **canaryResultsRetention** (<code>[Duration](#aws-cdk-core-duration)</code>)  *No description* __*Optional*__
  * **canaryScheduleRate** (<code>[Duration](#aws-cdk-core-duration)</code>)  *No description* __*Optional*__
  * **canarySuccessRateLowerThreshold** (<code>number</code>)  *No description* __*Optional*__
  * **canaryTimeout** (<code>[Duration](#aws-cdk-core-duration)</code>)  *No description* __*Optional*__
  * **canaryTracingEnabled** (<code>boolean</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description 
-----|------|-------------
**canary** | <code>[Canary](#aws-cdk-aws-synthetics-canary)</code> | <span></span>
**canaryBucket** | <code>[IBucket](#aws-cdk-aws-s3-ibucket)</code> | <span></span>
**canaryLogs** | <code>[LogGroup](#aws-cdk-aws-logs-loggroup)</code> | <span></span>
**canarySuccessRateAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>
**canaryResultsRetention**? | <code>[Duration](#aws-cdk-core-duration)</code> | __*Optional*__



## struct WebCanaryProps  <a id="softcomweb-ec-cdkpatterns-webcanary-webcanaryprops"></a>






Name | Type | Description 
-----|------|-------------
**canaryBucket** | <code>[IBucket](#aws-cdk-aws-s3-ibucket)</code> | <span></span>
**canaryRuntime** | <code>[Runtime](#aws-cdk-aws-synthetics-runtime)</code> | <span></span>
**canaryTest** | <code>[Test](#aws-cdk-aws-synthetics-test)</code> | <span></span>
**canaryBucketName**? | <code>string</code> | __*Optional*__
**canaryBucketPrefix**? | <code>string</code> | __*Optional*__
**canaryEnvironmentVariables**? | <code>Map<string, string></code> | __*Optional*__
**canaryMemory**? | <code>number</code> | __*Optional*__
**canaryName**? | <code>string</code> | __*Optional*__
**canaryResultsRetention**? | <code>[Duration](#aws-cdk-core-duration)</code> | __*Optional*__
**canaryScheduleRate**? | <code>[Duration](#aws-cdk-core-duration)</code> | __*Optional*__
**canarySuccessRateLowerThreshold**? | <code>number</code> | __*Optional*__
**canaryTimeout**? | <code>[Duration](#aws-cdk-core-duration)</code> | __*Optional*__
**canaryTracingEnabled**? | <code>boolean</code> | __*Optional*__



