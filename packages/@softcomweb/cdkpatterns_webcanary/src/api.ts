import * as cdk from "@aws-cdk/core";
import * as s3 from "@aws-cdk/aws-s3";
import * as iam from "@aws-cdk/aws-iam";
import * as logs from "@aws-cdk/aws-logs";
import * as synthetics from "@aws-cdk/aws-synthetics";
import * as cloudwatch from "@aws-cdk/aws-cloudwatch";

export interface WebCanaryProps {
  readonly canaryName?: string;
  readonly canaryBucket: s3.IBucket;
  readonly canaryBucketName?: string;
  readonly canaryBucketPrefix?: string;
  readonly canaryResultsRetention?: cdk.Duration;
  readonly canaryTimeout?: cdk.Duration;
  readonly canaryScheduleRate?: cdk.Duration;
  readonly canaryEnvironmentVariables?: Record<string, string>;
  readonly canaryRuntime: synthetics.Runtime;
  readonly canaryTest: synthetics.Test;
  readonly canaryTracingEnabled?: true;
  readonly canaryMemory?: number;
  readonly canarySuccessRateLowerThreshold?: number;
}

export class WebCanary extends cdk.Construct {
  public readonly canaryLogs: logs.LogGroup;
  public readonly canaryBucket: s3.IBucket;
  public readonly canary: synthetics.Canary;
  public readonly canarySuccessRateAlarm: cloudwatch.Alarm;

  readonly canaryResultsRetention?: cdk.Duration;

  constructor(scope: cdk.Construct, id: string, props: WebCanaryProps) {
    super(scope, id);

    this.canaryResultsRetention =
      props.canaryResultsRetention || cdk.Duration.days(7);

    this.canaryBucket =
      props.canaryBucket ||
      new s3.Bucket(this, "canaryBucket", {
        bucketName: props.canaryBucketName,
        accessControl: s3.BucketAccessControl.PRIVATE,
        autoDeleteObjects: true,
        encryption: s3.BucketEncryption.S3_MANAGED,
        removalPolicy: cdk.RemovalPolicy.DESTROY,
        versioned: false,
        lifecycleRules: [
          {
            id: "default",
            abortIncompleteMultipartUploadAfter: cdk.Duration.days(1),
            expiration: this.canaryResultsRetention,
            noncurrentVersionExpiration: this.canaryResultsRetention,
          },
        ],
      });

    this.canary = new synthetics.Canary(this, "canary", {
      canaryName: props.canaryName,
      artifactsBucketLocation: {
        bucket: this.canaryBucket as any,
        prefix: props.canaryBucketPrefix,
      },
      schedule: synthetics.Schedule.rate(
        props.canaryScheduleRate || (cdk.Duration.minutes(15) as any)
      ),
      test: props.canaryTest,
      runtime: props.canaryRuntime,
      environmentVariables: props.canaryEnvironmentVariables,
      failureRetentionPeriod: this.canaryResultsRetention as any,
      successRetentionPeriod: this.canaryResultsRetention as any,
      startAfterCreation: true,
    });
    this.canary.role.addManagedPolicy(
      iam.ManagedPolicy.fromAwsManagedPolicyName(
        "service-role/AWSLambdaBasicExecutionRole"
      )
    );
    this.canary.role.addManagedPolicy(
      iam.ManagedPolicy.fromAwsManagedPolicyName("AWSXrayWriteOnlyAccess")
    );

    this.canaryBucket.grantWrite(this.canary.role as any);

    // TODO(tk): This setting is not enabled in CDK so we need to override it.
    (this.canary.node.defaultChild as synthetics.CfnCanary).addPropertyOverride(
      "RunConfig.ActiveTracing",
      props.canaryTracingEnabled || true
    );
    (this.canary.node.defaultChild as synthetics.CfnCanary).addPropertyOverride(
      "RunConfig.MemoryInMB",
      props.canaryMemory || 960
    );
    (this.canary.node.defaultChild as synthetics.CfnCanary).addPropertyOverride(
      "RunConfig.TimeoutInSeconds",
      props.canaryTimeout || cdk.Duration.seconds(90).toSeconds()
    );

    this.canaryLogs = new logs.LogGroup(this, "canaryLogs", {
      logGroupName: `/aws/lambda/cwsyn-${this.canary.canaryName}-${this.canary.canaryId}`,
      retention: logs.RetentionDays.ONE_WEEK,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    this.canarySuccessRateAlarm = new cloudwatch.Alarm(
      this,
      id + "canarySuccessRateAlarm",
      {
        alarmName: props.canaryName
          ? `cwsyn/${this.canary.canaryName}-${this.canary.canaryId}/successRateAlarm`
          : undefined,
        metric: this.canary.metricSuccessPercent() as any,
        threshold: props.canarySuccessRateLowerThreshold || 90,
        comparisonOperator: cloudwatch.ComparisonOperator.LESS_THAN_THRESHOLD,
        evaluationPeriods: 2,
        alarmDescription: [
          `MEDIUM`,
          `Canary ${this.canary.canaryName} has a success rate below ${
            props.canarySuccessRateLowerThreshold || 90
          }%`,
          `Look into CloudWatch Logs (${this.canaryLogs.logGroupName}) and XRay for errors.`,
        ].join(" | "),
      }
    );
  }
}
