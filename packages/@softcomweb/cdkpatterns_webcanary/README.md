# @softcomweb-ec/cdkpatterns_starter

<!--BEGIN STABILITY BANNER-->

---
![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> CloudWatch Synthetics Canaries wrapper.

This module creates a Synthetics canary for monitoring webapps along with

- an S3 bucket for storing the data for Synthetics
- a CloudWatch logs group for storing handler logs
- a CloudWatch alarm for low success rate of the canary

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/cdkpatterns_canary
```

## How to Use

```js
import * as fs from "fs";
import * as cdk from "@aws-cdk/core";
import * as synthetics from "@aws-cdk/aws-synthetics";
import * as cdkpatterns_canary from "@softcomweb-ec/cdkpatterns_canary";

new cdkpatterns_canary.WebCanary(this, "webcanary", {
    canaryRuntime: synthetics.Runtime.SYNTHETICS_NODEJS_PUPPETEER_3_2,
    canaryTest: {
        handler: "index.handler",
        // for node.js the path for non-inlined code must be nodejs/node_modules/index.js
        code: synthetics.Code.fromInline(
          fs.readFileSync(path.join(__dirname, "canary", "nodejs", "node_modules", "index.js")).toString(),
        ),
    },
    canaryEnvironmentVariables: {
        FOO: "BAR",
    },
    canarySuccessRateLowerThreshold: 55,
})
```

## API Reference

See [API.md](API.md).