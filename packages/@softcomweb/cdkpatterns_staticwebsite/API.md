# API Reference

**Classes**

Name|Description
----|-----------
[Website](#softcomweb-ec-cdkpatterns-staticwebsite-website)|*No description*


**Structs**

Name|Description
----|-----------
[AddDomainNameProps](#softcomweb-ec-cdkpatterns-staticwebsite-adddomainnameprops)|*No description*
[WebsiteProps](#softcomweb-ec-cdkpatterns-staticwebsite-websiteprops)|*No description*



## class Website  <a id="softcomweb-ec-cdkpatterns-staticwebsite-website"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable)
__Extends__: [Construct](#aws-cdk-core-construct)

### Initializer




```ts
new Website(scope: Construct, id: string, props: WebsiteProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[WebsiteProps](#softcomweb-ec-cdkpatterns-staticwebsite-websiteprops)</code>)  *No description*
  * **distributionCertificate** (<code>[ICertificate](#aws-cdk-aws-certificatemanager-icertificate)</code>)  Certificate to use with CloudFront. __*Default*__: no certificate will be used
  * **distributionDomainNames** (<code>Array<string></code>)  A list of domain names for the distribution. __*Default*__: no domain names
  * **distributionGeoRestriction** (<code>[GeoRestriction](#aws-cdk-aws-cloudfront-georestriction)</code>)  Georestriction for CloudFront. __*Default*__: DE
  * **webAccessLogBucket** (<code>[IBucket](#aws-cdk-aws-s3-ibucket)</code>)  An existing S3 bucket to store logs. __*Default*__: A new bucket will created
  * **webAccessLogBucketPrefix** (<code>string</code>)  *No description* __*Default*__: no prefix
  * **webAccessLogEnabled** (<code>boolean</code>)  Enables web access logs. __*Default*__: true
  * **webAccessLogExpiration** (<code>[Duration](#aws-cdk-core-duration)</code>)  Retention for all access logs. __*Default*__: 7 days
  * **webAssets** (<code>Array<[ISource](#aws-cdk-aws-s3-deployment-isource)></code>)  A list of assets to upload to the S3 bucket. __*Optional*__
  * **webAssetsExpiration** (<code>[Duration](#aws-cdk-core-duration)</code>)  Retention for web assets (s3 objects). __*Default*__: no expiration
  * **webRoot** (<code>string</code>)  Root to service with CloudFront. __*Default*__: index.html



### Properties


Name | Type | Description 
-----|------|-------------
**webAccessLogBucket** | <code>[IBucket](#aws-cdk-aws-s3-ibucket)</code> | <span></span>
**webDistribution** | <code>[Distribution](#aws-cdk-aws-cloudfront-distribution)</code> | <span></span>
**webDistribution4xxErrorRateAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>
**webDistribution5xxErrorRateAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>
**websiteBucket** | <code>[Bucket](#aws-cdk-aws-s3-bucket)</code> | <span></span>
**websiteBucket4xxErrorRateAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>
**websiteBucket5xxErrorRateAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>

### Methods


#### addDomainName(id, recordName, hostedZone, props?) <a id="softcomweb-ec-cdkpatterns-staticwebsite-website-adddomainname"></a>

Wrapper to create a DNS entry for the CloudFront distribution.

```ts
addDomainName(id: string, recordName: string, hostedZone: IHostedZone, props?: AddDomainNameProps): void
```

* **id** (<code>string</code>)  id of the domain name.
* **recordName** (<code>string</code>)  the record name to use for the DNS entry.
* **hostedZone** (<code>[IHostedZone](#aws-cdk-aws-route53-ihostedzone)</code>)  the Hosted Zone to create the DNS entry on.
* **props** (<code>[AddDomainNameProps](#softcomweb-ec-cdkpatterns-staticwebsite-adddomainnameprops)</code>)  configuration props.
  * **ttl** (<code>[Duration](#aws-cdk-core-duration)</code>)  *No description* __*Optional*__






## struct AddDomainNameProps  <a id="softcomweb-ec-cdkpatterns-staticwebsite-adddomainnameprops"></a>






Name | Type | Description 
-----|------|-------------
**ttl**? | <code>[Duration](#aws-cdk-core-duration)</code> | __*Optional*__



## struct WebsiteProps  <a id="softcomweb-ec-cdkpatterns-staticwebsite-websiteprops"></a>






Name | Type | Description 
-----|------|-------------
**distributionCertificate**? | <code>[ICertificate](#aws-cdk-aws-certificatemanager-icertificate)</code> | Certificate to use with CloudFront.<br/>__*Default*__: no certificate will be used
**distributionDomainNames**? | <code>Array<string></code> | A list of domain names for the distribution.<br/>__*Default*__: no domain names
**distributionGeoRestriction**? | <code>[GeoRestriction](#aws-cdk-aws-cloudfront-georestriction)</code> | Georestriction for CloudFront.<br/>__*Default*__: DE
**webAccessLogBucket**? | <code>[IBucket](#aws-cdk-aws-s3-ibucket)</code> | An existing S3 bucket to store logs.<br/>__*Default*__: A new bucket will created
**webAccessLogBucketPrefix**? | <code>string</code> | __*Default*__: no prefix
**webAccessLogEnabled**? | <code>boolean</code> | Enables web access logs.<br/>__*Default*__: true
**webAccessLogExpiration**? | <code>[Duration](#aws-cdk-core-duration)</code> | Retention for all access logs.<br/>__*Default*__: 7 days
**webAssets**? | <code>Array<[ISource](#aws-cdk-aws-s3-deployment-isource)></code> | A list of assets to upload to the S3 bucket.<br/>__*Optional*__
**webAssetsExpiration**? | <code>[Duration](#aws-cdk-core-duration)</code> | Retention for web assets (s3 objects).<br/>__*Default*__: no expiration
**webRoot**? | <code>string</code> | Root to service with CloudFront.<br/>__*Default*__: index.html



