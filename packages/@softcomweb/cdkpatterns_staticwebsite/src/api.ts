import * as certificatemanager from "@aws-cdk/aws-certificatemanager";
import * as cloudfront from "@aws-cdk/aws-cloudfront";
import * as cloudfrontorigins from "@aws-cdk/aws-cloudfront-origins";
import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as route53 from "@aws-cdk/aws-route53";
import * as route53targets from "@aws-cdk/aws-route53-targets";
import * as s3 from "@aws-cdk/aws-s3";
import * as s3deployment from "@aws-cdk/aws-s3-deployment";
import * as cdk from "@aws-cdk/core";

const CLOUDWATCH_NAMESPACE_CLOUDFRONT = "AWS/CloudFront";
const CLOUDWATCH_NAMESPACE_CLOUDFRONT_DISTRO_DIMENSIONS_DEFAULT = {
  Region: "Global",
};

export interface AddDomainNameProps {
  readonly ttl?: cdk.Duration;
}

export interface WebsiteProps {
  /**
   * Enables web access logs. If set to true, a new bucket will be created
   * where CloudFront and S3 store their logs to.
   *
   * @default true
   */
  readonly webAccessLogEnabled?: boolean;
  /**
   * An existing S3 bucket to store logs.
   *
   * @default A new bucket will created
   */
  readonly webAccessLogBucket?: s3.IBucket;
  /**
   * @default no prefix
   */
  readonly webAccessLogBucketPrefix?: string;
  /**
   * Retention for all access logs.
   *
   * @default 7 days
   */
  readonly webAccessLogExpiration?: cdk.Duration;
  /**
   * A list of assets to upload to the S3 bucket.
   */
  readonly webAssets?: s3deployment.ISource[];
  /**
   * Retention for web assets (s3 objects).
   *
   * @default no expiration
   */
  readonly webAssetsExpiration?: cdk.Duration;
  /**
   * Root to service with CloudFront
   *
   * @default index.html
   */
  readonly webRoot?: string;

  /**
   * Georestriction for CloudFront.
   *
   * @default DE
   */
  readonly distributionGeoRestriction?: cloudfront.GeoRestriction;
  /**
   * Certificate to use with CloudFront.
   *
   * @default no certificate will be used
   */
  readonly distributionCertificate?: certificatemanager.ICertificate;
  /**
   * A list of domain names for the distribution.
   *
   * @default no domain names
   */
  readonly distributionDomainNames?: string[];
}

export class Website extends cdk.Construct {
  public readonly websiteBucket: s3.Bucket;
  public readonly webDistribution: cloudfront.Distribution;
  public readonly webAccessLogBucket: s3.IBucket;

  public readonly websiteBucket4xxErrorRateAlarm: cloudwatch.Alarm;
  public readonly websiteBucket5xxErrorRateAlarm: cloudwatch.Alarm;
  public readonly webDistribution4xxErrorRateAlarm: cloudwatch.Alarm;
  public readonly webDistribution5xxErrorRateAlarm: cloudwatch.Alarm;

  constructor(scope: cdk.Construct, id: string, props: WebsiteProps) {
    super(scope, id);

    this.webAccessLogBucket =
      props.webAccessLogBucket ||
      new s3.Bucket(this, "webAccessLogBucket", {
        accessControl: s3.BucketAccessControl.LOG_DELIVERY_WRITE,
        blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
        encryption: s3.BucketEncryption.S3_MANAGED,
        enforceSSL: true,
        removalPolicy: cdk.RemovalPolicy.DESTROY,
        versioned: true,
        autoDeleteObjects: true,
        lifecycleRules: [
          {
            abortIncompleteMultipartUploadAfter: cdk.Duration.days(2),
            expiration: props.webAccessLogExpiration || cdk.Duration.days(7),
            noncurrentVersionExpiration:
              props.webAccessLogExpiration || cdk.Duration.days(7),
          },
        ],
      });

    const webRoot = props.webRoot || "index.html";

    this.websiteBucket = new s3.Bucket(this, "websiteBucket", {
      accessControl: s3.BucketAccessControl.PRIVATE,
      blockPublicAccess: {
        blockPublicAcls: true,
        blockPublicPolicy: true,
        ignorePublicAcls: true,
        restrictPublicBuckets: true,
      },
      encryption: s3.BucketEncryption.S3_MANAGED,
      enforceSSL: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      versioned: true,
      autoDeleteObjects: true,
      websiteIndexDocument: webRoot,
      websiteErrorDocument: webRoot,
      metrics: [
        {
          id: "default",
        },
      ],
      lifecycleRules: props.webAssetsExpiration
        ? [
            {
              abortIncompleteMultipartUploadAfter: cdk.Duration.days(2),
              expiration: props.webAssetsExpiration,
              noncurrentVersionExpiration: props.webAssetsExpiration,
            },
          ]
        : [],

      serverAccessLogsBucket: props.webAccessLogEnabled
        ? this.webAccessLogBucket
        : undefined,
      serverAccessLogsPrefix: props.webAccessLogEnabled
        ? `${props.webAccessLogBucketPrefix || "accessLogs"}/s3`
        : undefined,
    });

    const webBucket4xxErrorRateMetric = new cloudwatch.Metric({
      namespace: "AWS/S3",
      metricName: "4xxErrors",
      dimensionsMap: {
        BucketName: this.websiteBucket.bucketName,
      },
      statistic: "Average",
    });

    this.websiteBucket4xxErrorRateAlarm = new cloudwatch.Alarm(
      this,
      "websiteBucket4xxErrorRateAlarm",
      {
        evaluationPeriods: 1,
        metric: webBucket4xxErrorRateMetric,
        threshold: 10,
        alarmDescription: [
          "MEDIUM",
          `The S3 bucket ${this.websiteBucket.bucketName} has an error rate for client errors (4xx) > 10%`,
          `Check the access logs (bucket: ${this.webAccessLogBucket.bucketName}) to find the issue`,
        ].join(" | "),
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }
    );

    const webBucket5xxErrorRateMetric = new cloudwatch.Metric({
      namespace: "AWS/S3",
      metricName: "5xxErrors",
      dimensionsMap: {
        BucketName: this.websiteBucket.bucketName,
      },
      statistic: "Average",
    });

    this.websiteBucket5xxErrorRateAlarm = new cloudwatch.Alarm(
      this,
      "websiteBucket5xxErrorRateAlarm",
      {
        evaluationPeriods: 1,
        metric: webBucket5xxErrorRateMetric,
        threshold: 1,
        alarmDescription: [
          "HIGH",
          `The S3 bucket ${this.websiteBucket.bucketName} has an error rate for server errors (5xx) > 1%`,
          `Check the access logs (bucket: ${this.webAccessLogBucket.bucketName}) to find the issue`,
        ].join(" | "),
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }
    );

    this.webDistribution = new cloudfront.Distribution(
      this,
      "webDistribution",
      {
        defaultBehavior: {
          origin: new cloudfrontorigins.S3Origin(this.websiteBucket, {
            originPath: "/",
            originAccessIdentity: new cloudfront.OriginAccessIdentity(
              this,
              "webDistributionOAI",
              {
                comment: `OAI for ${this.node.path}`,
              }
            ),
          }),
          allowedMethods: cloudfront.AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
          cachePolicy: cloudfront.CachePolicy.CACHING_OPTIMIZED,
          originRequestPolicy: cloudfront.OriginRequestPolicy.ALL_VIEWER,
          viewerProtocolPolicy:
            cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
        },
        errorResponses: [404, 403].map((httpStatus) => ({
          httpStatus,
          responseHttpStatus: 200,
          responsePagePath: webRoot,
        })),
        defaultRootObject: webRoot,
        enabled: true,
        geoRestriction:
          props.distributionGeoRestriction ||
          cloudfront.GeoRestriction.allowlist("DE"),
        minimumProtocolVersion: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2018,
        enableLogging: props.webAccessLogEnabled,
        logBucket: props.webAccessLogEnabled
          ? this.webAccessLogBucket
          : undefined,
        logFilePrefix: props.webAccessLogEnabled
          ? `${props.webAccessLogBucketPrefix || "accessLogs"}/cloudFront`
          : undefined,
        logIncludesCookies: false,
        certificate: props.distributionCertificate,
        domainNames: props.distributionDomainNames,
        comment: `CDN for ${this.node.path}`,
        priceClass: cloudfront.PriceClass.PRICE_CLASS_100,
      }
    );

    const webDistribution4xxErrorRateMetric = new cloudwatch.Metric({
      namespace: CLOUDWATCH_NAMESPACE_CLOUDFRONT,
      metricName: "4xxErrorRate",
      dimensionsMap: {
        DistributionId: this.webDistribution.distributionId,
        ...CLOUDWATCH_NAMESPACE_CLOUDFRONT_DISTRO_DIMENSIONS_DEFAULT,
      },
      statistic: "Average",
    });

    this.webDistribution4xxErrorRateAlarm = new cloudwatch.Alarm(
      this,
      "webDistribution4xxErrorRateAlarm",
      {
        evaluationPeriods: 1,
        metric: webDistribution4xxErrorRateMetric,
        threshold: 0.5,
        alarmDescription: [
          "HIGH",
          `The CloudFront distribution ${this.webDistribution.distributionId} has an error rate for client errors (4xx) > 50%`,
          `Check the access logs (bucket: ${this.webAccessLogBucket.bucketName}) to find the issue`,
        ].join(" | "),
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }
    );

    const webDistribution5xxErrorRateMetric = new cloudwatch.Metric({
      namespace: CLOUDWATCH_NAMESPACE_CLOUDFRONT,
      metricName: "5xxErrorRate",
      dimensionsMap: {
        DistributionId: this.webDistribution.distributionId,
        ...CLOUDWATCH_NAMESPACE_CLOUDFRONT_DISTRO_DIMENSIONS_DEFAULT,
      },
      statistic: "Average",
    });

    this.webDistribution5xxErrorRateAlarm = new cloudwatch.Alarm(
      this,
      "webDistribution5xxErrorRateAlarm",
      {
        evaluationPeriods: 1,
        metric: webDistribution5xxErrorRateMetric,
        threshold: 0.05,
        alarmDescription: [
          "HIGH",
          `The CloudFront distribution ${this.webDistribution.distributionId} has an error rate for server errors (5xx) > 5%`,
          `Check the access logs (bucket: ${this.webAccessLogBucket.bucketName}) to find the issue`,
        ].join(" | "),
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }
    );

    if (props.webAssets) {
      new s3deployment.BucketDeployment(this, "websiteBucketDeployment", {
        destinationBucket: this.websiteBucket,
        distribution: this.webDistribution,
        sources: [...props.webAssets],
      });
    }
  }

  /**
   * Wrapper to create a DNS entry for the CloudFront distribution.
   *
   * @param id id of the domain name
   * @param recordName the record name to use for the DNS entry
   * @param hostedZone the Hosted Zone to create the DNS entry on
   * @param props configuration props
   */
  public addDomainName(
    id: string,
    recordName: string,
    hostedZone: route53.IHostedZone,
    props?: AddDomainNameProps
  ): void {
    new route53.ARecord(this, id, {
      zone: hostedZone,
      recordName: recordName,
      target: route53.RecordTarget.fromAlias(
        new route53targets.CloudFrontTarget(this.webDistribution)
      ),
      ttl: props?.ttl,
    });
  }
}
