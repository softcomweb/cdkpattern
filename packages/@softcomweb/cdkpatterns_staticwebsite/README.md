# @softcomweb-ec/cdkpatterns_staticwebsite

<!--BEGIN STABILITY BANNER-->

---

![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> A CDK construct for serving static website content over S3 and CloudFront.

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/si_cdkpatterns_staticwebsite
```

## How to Use

```ts
import * as cdk from "@aws-cdk/core";
import * as si_cdkpatterns_staticwebsite from "@softcomweb-ec/cdkpatterns_staticwebsite";

new cdkpatterns_staticwebsite.Website(this, "website", {

});
```

This will create the following resources:

- An S3 bucket for hosting your static assets
- A CloudFront distribution for serving assets over HTTPS and a CDN
- CloudWatch alarms for 4xx and 5xx errors
- An S3 bucket for access logging (not enabled by default)

### Uploading assets

If you want to upload assets on every deployment to the S3 bucket:

```ts
import * as cdk from "@aws-cdk/core";
import * as s3deployment from "@aws-cdk/aws-s3-deployment";
import * as si_cdkpatterns_staticwebsite from "@softcomweb-ec/cdkpatterns_staticwebsite";

Website(stack, "website", {
    webAssets: [
      s3deployment.Source.asset(`${__dirname}/assets.d`),
    ]
});
```

## API Reference

See [API.md](API.md).