import {
  AwsCustomResource,
  AwsCustomResourcePolicy,
  AwsSdkCall,
  PhysicalResourceId,
} from "aws-cdk-lib/custom-resources";
import { Construct } from "constructs";
import { RetentionDays } from "aws-cdk-lib/aws-logs";
import { Duration } from "aws-cdk-lib";
import { Effect, PolicyStatement } from "aws-cdk-lib/aws-iam";
import { dataToItem } from "dynamo-converters";

export interface BatchPutItemsResourceConstructProps {
  tableName: string;
  tableArn: string;
  items: any[];
}

interface RequestItem {
  [key: string]: any[];
}

interface DynamoInsert {
  RequestItems: RequestItem;
}

export class BatchPutItemsResourceConstruct extends Construct {
  constructor(
    scope: Construct,
    id: string,
    props: BatchPutItemsResourceConstructProps
  ) {
    super(scope, id);
    this.insertMultipleRecord(
      props.tableName,
      props.tableArn,
      props.items.map(dataToItem)
    );
  }

  private insertMultipleRecord(
    tableName: string,
    tableArn: string,
    items: any[]
  ) {
    const records = this.constructBatchInsertObject(items, tableName);

    const awsSdkCall: AwsSdkCall = {
      service: "DynamoDB",
      action: "batchWriteItem",
      physicalResourceId: PhysicalResourceId.of(tableName + "insert"),
      parameters: records,
    };

    new AwsCustomResource(this, "InitialTableData_custom_resource", {
      onCreate: awsSdkCall,
      onUpdate: awsSdkCall,
      logRetention: RetentionDays.ONE_WEEK,
      policy: AwsCustomResourcePolicy.fromStatements([
        new PolicyStatement({
          sid: "DynamoWriteAccess",
          effect: Effect.ALLOW,
          actions: ["dynamodb:BatchWriteItem"],
          resources: [tableArn],
        }),
      ]),
      timeout: Duration.minutes(5),
    });
  }

  private constructBatchInsertObject(items: any[], tableName: string) {
    const itemsAsDynamoPutRequest: any[] = [];
    items.forEach((item) =>
      itemsAsDynamoPutRequest.push({
        PutRequest: {
          Item: item,
        },
      })
    );
    const records: DynamoInsert = {
      RequestItems: {},
    };
    records.RequestItems[tableName] = itemsAsDynamoPutRequest;
    return records;
  }
}
