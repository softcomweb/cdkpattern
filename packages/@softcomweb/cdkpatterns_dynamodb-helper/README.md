# @softcomweb-ec/ssm-helper

SSM Helper to define custom dummy values during synth time.

## API Reference

See [API.md](API.md).