import * as cdk from "aws-cdk-lib";
import { cloud_assembly_schema as cxschema } from "aws-cdk-lib";

export const MOCK_CLUSTER_PARAMS = `{
  "vpcName": "mocked-vpc",
  "clusterName": "mocked-cluster",
  "clusterEndpoint": "https://00000000000.grx.eu-central-1.eks.amazonaws.com",
  "clusterCertificateAuthorityData": "XXX",
  "clusterSecurityGroupId": "sg-xxxxx",
  "clusterEncryptionConfigKeyArn": "",
  "kubectlRoleArn": "arn:aws:iam::00000000000:role/mock-controlplane-eksClusterCreationRolexxxx-00000000000",
  "openIdConnectProviderArn": "arn:aws:iam::00000000000:oidc-provider/oidc.eks.eu-central-1.amazonaws.com/id/00000000000"
}`;

export function ssmParameterLookupWithDummyValue<T>(
  scope: any,
  parameterName: string,
  dummyValue: T
): T {
  return cdk.ContextProvider.getValue(scope, {
    provider: cxschema.ContextProvider.SSM_PARAMETER_PROVIDER,
    props: {
      parameterName,
    },
    dummyValue: dummyValue,
  }).value;
}
