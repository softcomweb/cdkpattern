import * as cdk from "@aws-cdk/core";
import * as logs from "@aws-cdk/aws-logs";
import { AspectBase } from "./aspect-base";

export class AspectLogGroupRemoval extends AspectBase {
  visit(node: cdk.IConstruct): void {
    if (node instanceof logs.CfnLogGroup) {
      const resource = node as logs.CfnLogGroup;
      if (this.fixOnError) {
        resource.applyRemovalPolicy(cdk.RemovalPolicy.DESTROY);
      }
    }
  }
}
