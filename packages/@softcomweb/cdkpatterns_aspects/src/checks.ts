import * as cdk from "@aws-cdk/core";
import { AwsSolutionsChecks } from "cdk-nag";
import { AspectBase, AspectBaseProps } from "./aspect-base";
import { AspectDynamoDBTableRemoval } from "./aspect-dynamodbtable-removal";
import { AspectDynamoDBTableSSE } from "./aspect-dynamodbtable-sse";
import { AspectLogGroupRemoval } from "./aspect-loggroup-removal";
import { AspectLogGroupRetention } from "./aspect-loggroup-retention";
import { AspectS3BucketRemoval } from "./aspect-s3bucket-removal";
import { AspectS3BucketSSE } from "./aspect-s3bucket-sse";
import { AspectKmsKeyRemoval } from "./aspect-kmskey-removal";
import { AspectKmsKeyRotation } from "./aspect-kmskey-rotation";

export interface softcomwebChecksProps extends AspectBaseProps {}

export class softcomwebChecks extends AspectBase {
  constructor(props: softcomwebChecksProps) {
    super(props);
  }

  visit(node: cdk.IConstruct): void {
    new AspectLogGroupRemoval({
      fixOnError: this.fixOnError,
    }).visit(node);

    new AspectLogGroupRetention({
      fixOnError: this.fixOnError,
    }).visit(node);

    new AspectS3BucketRemoval({
      fixOnError: this.fixOnError,
    }).visit(node);

    new AspectS3BucketSSE({
      fixOnError: this.fixOnError,
    }).visit(node);

    new AspectDynamoDBTableRemoval({
      fixOnError: this.fixOnError,
    }).visit(node);

    new AspectDynamoDBTableSSE({
      fixOnError: this.fixOnError,
    }).visit(node);

    new AspectKmsKeyRemoval({
      fixOnError: this.fixOnError,
    }).visit(node);

    new AspectKmsKeyRotation({
      fixOnError: this.fixOnError,
    }).visit(node);
  }
}

/**
 * Wrapper around https://github.com/cdklabs/cdk-nag.
 */
export class AwsSolutionsChecksWrapper extends AwsSolutionsChecks {
  constructor(props: AspectBaseProps) {
    super({
      verbose: true,
    });

    props;
  }
}
