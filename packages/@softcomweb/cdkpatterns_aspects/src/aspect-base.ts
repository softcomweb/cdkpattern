import * as cdk from "@aws-cdk/core";

export interface AspectBaseProps {
  readonly fixOnError?: boolean;
}

export abstract class AspectBase implements cdk.IAspect {
  public readonly fixOnError: boolean;

  constructor(props: AspectBaseProps) {
    this.fixOnError = props.fixOnError || false;
  }

  abstract visit(node: cdk.IConstruct): void;
}
