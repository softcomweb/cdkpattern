import * as cdk from "@aws-cdk/core";
import * as logs from "@aws-cdk/aws-logs";
import { AspectBaseProps, AspectBase } from "./aspect-base";

export interface AspectLogGroupRetentionProps extends AspectBaseProps {
  readonly defaultRetention?: logs.RetentionDays;
}

export class AspectLogGroupRetention extends AspectBase {
  private readonly defaultRetention: logs.RetentionDays;

  constructor(props: AspectLogGroupRetentionProps) {
    super(props);
    this.defaultRetention =
      props.defaultRetention || logs.RetentionDays.ONE_WEEK;
  }

  visit(node: cdk.IConstruct): void {
    if (node instanceof logs.CfnLogGroup) {
      const resource = node as logs.CfnLogGroup;
      if (resource.retentionInDays === undefined) {
        if (this.fixOnError) {
          resource.retentionInDays = this.defaultRetention;
        } else {
          cdk.Annotations.of(resource).addWarning(
            "Log group has infinite retention."
          );
        }
      }
    }
  }
}
