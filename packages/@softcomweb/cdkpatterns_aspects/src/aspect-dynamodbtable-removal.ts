import * as cdk from "@aws-cdk/core";
import * as dynamodb from "@aws-cdk/aws-dynamodb";
import { AspectBase } from "./aspect-base";

export class AspectDynamoDBTableRemoval extends AspectBase {
  visit(node: cdk.IConstruct): void {
    if (node instanceof dynamodb.CfnTable) {
      const resource = node as dynamodb.CfnTable;
      if (this.fixOnError) {
        resource.applyRemovalPolicy(cdk.RemovalPolicy.DESTROY);
      }
    }
  }
}
