import * as cdk from "@aws-cdk/core";
import * as kms from "@aws-cdk/aws-kms";
import { AspectBase } from "./aspect-base";

export class AspectKmsKeyRotation extends AspectBase {
  visit(node: cdk.IConstruct): void {
    if (node instanceof kms.CfnKey) {
      const resource = node as kms.CfnKey;
      if (resource.enableKeyRotation === undefined) {
        if (this.fixOnError) {
          resource.enableKeyRotation = true;
        } else {
          cdk.Annotations.of(resource).addWarning(
            "the kms key does not have rotation enabled"
          );
        }
      }
    }
  }
}
