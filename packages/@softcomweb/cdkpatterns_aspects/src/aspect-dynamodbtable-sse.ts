import * as cdk from "@aws-cdk/core";
import * as dynamodb from "@aws-cdk/aws-dynamodb";
import { AspectBase, AspectBaseProps } from "./aspect-base";

export interface AspectDynamoDBTableSSEProps extends AspectBaseProps {
  readonly kmsMasterKeyId?: string;
}

export class AspectDynamoDBTableSSE extends AspectBase {
  readonly kmsMasterKeyId: string;

  constructor(props: AspectDynamoDBTableSSEProps) {
    super(props);
    this.kmsMasterKeyId = props.kmsMasterKeyId || "alias/aws/dynamodb";
  }

  visit(node: cdk.IConstruct): void {
    if (node instanceof dynamodb.CfnTable) {
      const resource = node as dynamodb.CfnTable;
      if (resource.sseSpecification === undefined) {
        if (this.fixOnError) {
          resource.sseSpecification = {
            sseEnabled: true,
            sseType: "KMS", // only KMS is supported
            kmsMasterKeyId: this.kmsMasterKeyId,
          };
        } else {
          cdk.Annotations.of(resource).addWarning(
            "the dynamodb table does not have SSE enabled"
          );
        }
      }
    }
  }
}
