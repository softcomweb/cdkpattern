import * as cdk from "@aws-cdk/core";
import * as kms from "@aws-cdk/aws-kms";
import { AspectBase } from "./aspect-base";

export class AspectKmsKeyRemoval extends AspectBase {
  visit(node: cdk.IConstruct): void {
    if (node instanceof kms.CfnKey) {
      const resource = node as kms.CfnKey;
      if (this.fixOnError) {
        resource.applyRemovalPolicy(cdk.RemovalPolicy.DESTROY);
      }
    }
  }
}
