import * as cdk from "@aws-cdk/core";
import * as s3 from "@aws-cdk/aws-s3";
import { AspectBase } from "./aspect-base";

export class AspectS3BucketRemoval extends AspectBase {
  visit(node: cdk.IConstruct): void {
    if (node instanceof s3.CfnBucket) {
      const resource = node as s3.CfnBucket;
      if (this.fixOnError) {
        resource.applyRemovalPolicy(cdk.RemovalPolicy.DESTROY);
      }
    }
  }
}
