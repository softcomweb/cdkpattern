import * as cdk from "@aws-cdk/core";
import * as s3 from "@aws-cdk/aws-s3";
import { AspectBase } from "./aspect-base";

export class AspectS3BucketSSE extends AspectBase {
  visit(node: cdk.IConstruct): void {
    if (node instanceof s3.CfnBucket) {
      const resource = node as s3.CfnBucket;
      if (this.fixOnError) {
        resource.bucketEncryption = {
          serverSideEncryptionConfiguration: [
            {
              bucketKeyEnabled: true,
              serverSideEncryptionByDefault: {
                sseAlgorithm: "AES256",
              },
            },
          ],
        };
      } else {
        cdk.Annotations.of(resource).addWarning(
          "the bucket does not have SSE enabled"
        );
      }
    }
  }
}
