# API Reference

**Classes**

Name|Description
----|-----------
[AspectBase](#softcomweb-ec-cdkpatterns-aspects-aspectbase)|*No description*
[AspectDynamoDBTableRemoval](#softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtableremoval)|*No description*
[AspectDynamoDBTableSSE](#softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtablesse)|*No description*
[AspectLogGroupRemoval](#softcomweb-ec-cdkpatterns-aspects-aspectloggroupremoval)|*No description*
[AspectS3BucketRemoval](#softcomweb-ec-cdkpatterns-aspects-aspects3bucketremoval)|*No description*
[AspectS3BucketSSE](#softcomweb-ec-cdkpatterns-aspects-aspects3bucketsse)|*No description*
[AwsSolutionsChecksWrapper](#softcomweb-ec-cdkpatterns-aspects-awssolutionscheckswrapper)|Wrapper around https://github.com/cdklabs/cdk-nag.
[softcomwebChecks](#softcomweb-ec-cdkpatterns-aspects-softcomwebchecks)|*No description*


**Structs**

Name|Description
----|-----------
[AspectBaseProps](#softcomweb-ec-cdkpatterns-aspects-aspectbaseprops)|*No description*
[AspectDynamoDBTableSSEProps](#softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtablesseprops)|*No description*
[softcomwebChecksProps](#softcomweb-ec-cdkpatterns-aspects-softcomwebchecksprops)|*No description*



## class AspectBase  <a id="softcomweb-ec-cdkpatterns-aspects-aspectbase"></a>



__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Implemented by__: [AspectDynamoDBTableRemoval](#softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtableremoval), [AspectDynamoDBTableSSE](#softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtablesse), [AspectLogGroupRemoval](#softcomweb-ec-cdkpatterns-aspects-aspectloggroupremoval), [AspectS3BucketRemoval](#softcomweb-ec-cdkpatterns-aspects-aspects3bucketremoval), [AspectS3BucketSSE](#softcomweb-ec-cdkpatterns-aspects-aspects3bucketsse), [softcomwebChecks](#softcomweb-ec-cdkpatterns-aspects-softcomwebchecks)

### Initializer




```ts
new AspectBase(props: AspectBaseProps)
```

* **props** (<code>[AspectBaseProps](#softcomweb-ec-cdkpatterns-aspects-aspectbaseprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description 
-----|------|-------------
**fixOnError** | <code>boolean</code> | <span></span>

### Methods


#### visit(node) <a id="softcomweb-ec-cdkpatterns-aspects-aspectbase-visit"></a>

All aspects can visit an IConstruct.

```ts
visit(node: IConstruct): void
```

* **node** (<code>[IConstruct](#aws-cdk-core-iconstruct)</code>)  *No description*






## class AspectDynamoDBTableRemoval  <a id="softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtableremoval"></a>



__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Extends__: [AspectBase](#softcomweb-ec-cdkpatterns-aspects-aspectbase)

### Initializer




```ts
new AspectDynamoDBTableRemoval(props: AspectBaseProps)
```

* **props** (<code>[AspectBaseProps](#softcomweb-ec-cdkpatterns-aspects-aspectbaseprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__


### Methods


#### visit(node) <a id="softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtableremoval-visit"></a>

All aspects can visit an IConstruct.

```ts
visit(node: IConstruct): void
```

* **node** (<code>[IConstruct](#aws-cdk-core-iconstruct)</code>)  *No description*






## class AspectDynamoDBTableSSE  <a id="softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtablesse"></a>



__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Extends__: [AspectBase](#softcomweb-ec-cdkpatterns-aspects-aspectbase)

### Initializer




```ts
new AspectDynamoDBTableSSE(props: AspectDynamoDBTableSSEProps)
```

* **props** (<code>[AspectDynamoDBTableSSEProps](#softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtablesseprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__
  * **kmsMasterKeyId** (<code>string</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description 
-----|------|-------------
**kmsMasterKeyId** | <code>string</code> | <span></span>

### Methods


#### visit(node) <a id="softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtablesse-visit"></a>

All aspects can visit an IConstruct.

```ts
visit(node: IConstruct): void
```

* **node** (<code>[IConstruct](#aws-cdk-core-iconstruct)</code>)  *No description*






## class AspectLogGroupRemoval  <a id="softcomweb-ec-cdkpatterns-aspects-aspectloggroupremoval"></a>



__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Extends__: [AspectBase](#softcomweb-ec-cdkpatterns-aspects-aspectbase)

### Initializer




```ts
new AspectLogGroupRemoval(props: AspectBaseProps)
```

* **props** (<code>[AspectBaseProps](#softcomweb-ec-cdkpatterns-aspects-aspectbaseprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__


### Methods


#### visit(node) <a id="softcomweb-ec-cdkpatterns-aspects-aspectloggroupremoval-visit"></a>

All aspects can visit an IConstruct.

```ts
visit(node: IConstruct): void
```

* **node** (<code>[IConstruct](#aws-cdk-core-iconstruct)</code>)  *No description*






## class AspectS3BucketRemoval  <a id="softcomweb-ec-cdkpatterns-aspects-aspects3bucketremoval"></a>



__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Extends__: [AspectBase](#softcomweb-ec-cdkpatterns-aspects-aspectbase)

### Initializer




```ts
new AspectS3BucketRemoval(props: AspectBaseProps)
```

* **props** (<code>[AspectBaseProps](#softcomweb-ec-cdkpatterns-aspects-aspectbaseprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__


### Methods


#### visit(node) <a id="softcomweb-ec-cdkpatterns-aspects-aspects3bucketremoval-visit"></a>

All aspects can visit an IConstruct.

```ts
visit(node: IConstruct): void
```

* **node** (<code>[IConstruct](#aws-cdk-core-iconstruct)</code>)  *No description*






## class AspectS3BucketSSE  <a id="softcomweb-ec-cdkpatterns-aspects-aspects3bucketsse"></a>



__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Extends__: [AspectBase](#softcomweb-ec-cdkpatterns-aspects-aspectbase)

### Initializer




```ts
new AspectS3BucketSSE(props: AspectBaseProps)
```

* **props** (<code>[AspectBaseProps](#softcomweb-ec-cdkpatterns-aspects-aspectbaseprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__


### Methods


#### visit(node) <a id="softcomweb-ec-cdkpatterns-aspects-aspects3bucketsse-visit"></a>

All aspects can visit an IConstruct.

```ts
visit(node: IConstruct): void
```

* **node** (<code>[IConstruct](#aws-cdk-core-iconstruct)</code>)  *No description*






## class AwsSolutionsChecksWrapper  <a id="softcomweb-ec-cdkpatterns-aspects-awssolutionscheckswrapper"></a>

Wrapper around https://github.com/cdklabs/cdk-nag.

__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Extends__: [AwsSolutionsChecks](#cdk-nag-awssolutionschecks)

### Initializer




```ts
new AwsSolutionsChecksWrapper(props: AspectBaseProps)
```

* **props** (<code>[AspectBaseProps](#softcomweb-ec-cdkpatterns-aspects-aspectbaseprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__




## class softcomwebChecks  <a id="softcomweb-ec-cdkpatterns-aspects-softcomwebchecks"></a>



__Implements__: [IAspect](#aws-cdk-core-iaspect)
__Extends__: [AspectBase](#softcomweb-ec-cdkpatterns-aspects-aspectbase)

### Initializer




```ts
new softcomwebChecks(props: softcomwebChecksProps)
```

* **props** (<code>[softcomwebChecksProps](#softcomweb-ec-cdkpatterns-aspects-softcomwebchecksprops)</code>)  *No description*
  * **fixOnError** (<code>boolean</code>)  *No description* __*Optional*__


### Methods


#### visit(node) <a id="softcomweb-ec-cdkpatterns-aspects-softcomwebchecks-visit"></a>

All aspects can visit an IConstruct.

```ts
visit(node: IConstruct): void
```

* **node** (<code>[IConstruct](#aws-cdk-core-iconstruct)</code>)  *No description*






## struct AspectBaseProps  <a id="softcomweb-ec-cdkpatterns-aspects-aspectbaseprops"></a>






Name | Type | Description 
-----|------|-------------
**fixOnError**? | <code>boolean</code> | __*Optional*__



## struct AspectDynamoDBTableSSEProps  <a id="softcomweb-ec-cdkpatterns-aspects-aspectdynamodbtablesseprops"></a>






Name | Type | Description 
-----|------|-------------
**fixOnError**? | <code>boolean</code> | __*Optional*__
**kmsMasterKeyId**? | <code>string</code> | __*Optional*__



## struct softcomwebChecksProps  <a id="softcomweb-ec-cdkpatterns-aspects-softcomwebchecksprops"></a>






Name | Type | Description 
-----|------|-------------
**fixOnError**? | <code>boolean</code> | __*Optional*__



