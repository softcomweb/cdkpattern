# @softcomweb-ec/cdkpatterns_aspects

<!--BEGIN STABILITY BANNER-->

---
![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> Useful Aspects for dealing with retention policies and AwsGuard checks.

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/cdkpatterns_aspects
```

## How to Use

```js
import * as cdk from "@aws-cdk/core";
import * as cdkpatterns_aspects from "@softcomweb-ec/cdkpatterns_aspects";

const app = new cdk.App();

cdk.Aspects.of(app).add(new cdkpatterns_aspects.softcomwebChecks({
    fixOnError: true, // will auto-correct configurations
}));
```

You can also enable only some aspects if you want by using the exported aspect classes.

### CDK Nag

This package also wraps [cdklabs/cdk-nag](https://github.com/cdklabs/cdk-nag) for easier use.

```js
import * as cdk from "@aws-cdk/core";
import * as cdkpatterns_aspects from "@softcomweb-ec/cdkpatterns_aspects";

const app = new cdk.App();

cdk.Aspects.of(app).add(new cdkpatterns_aspects.AwsSolutionsChecksWrapper({
    
}));
```

## API Reference

See [API.md](API.md).