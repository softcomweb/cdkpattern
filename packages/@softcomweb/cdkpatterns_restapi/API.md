# API Reference

**Classes**

Name|Description
----|-----------
[RestApi](#softcomweb-ec-cdkpatterns-restapi-restapi)|*No description*


**Structs**

Name|Description
----|-----------
[RestApiProps](#softcomweb-ec-cdkpatterns-restapi-restapiprops)|*No description*



## class RestApi  <a id="softcomweb-ec-cdkpatterns-restapi-restapi"></a>



__Implements__: [IConstruct](#constructs-iconstruct), [IConstruct](#aws-cdk-core-iconstruct), [IConstruct](#constructs-iconstruct), [IDependable](#aws-cdk-core-idependable)
__Extends__: [Construct](#aws-cdk-core-construct)

### Initializer




```ts
new RestApi(scope: Construct, id: string, props: RestApiProps)
```

* **scope** (<code>[Construct](#aws-cdk-core-construct)</code>)  *No description*
* **id** (<code>string</code>)  *No description*
* **props** (<code>[RestApiProps](#softcomweb-ec-cdkpatterns-restapi-restapiprops)</code>)  *No description*
  * **restApiAccessLogFormat** (<code>[AccessLogFormat](#aws-cdk-aws-apigateway-accesslogformat)</code>)  *No description* __*Optional*__
  * **restApiAuthorizer** (<code>[Authorizer](#aws-cdk-aws-apigateway-authorizer)</code>)  *No description* __*Optional*__
  * **restApiCanaryEnabled** (<code>boolean</code>)  *No description* __*Optional*__
  * **restApiCanaryTrafficPercent** (<code>number</code>)  *No description* __*Optional*__
  * **restApiDataTraceEnabled** (<code>boolean</code>)  *No description* __*Optional*__
  * **restApiDefaultIntegration** (<code>[Integration](#aws-cdk-aws-apigateway-integration)</code>)  *No description* __*Optional*__
  * **restApiLogLevel** (<code>[MethodLoggingLevel](#aws-cdk-aws-apigateway-methodlogginglevel)</code>)  *No description* __*Optional*__
  * **restApiLogsRetenion** (<code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code>)  *No description* __*Optional*__
  * **restApiMethodOptions** (<code>Map<string, [MethodDeploymentOptions](#aws-cdk-aws-apigateway-methoddeploymentoptions)></code>)  *No description* __*Optional*__
  * **restApiMetricsEnabled** (<code>boolean</code>)  *No description* __*Optional*__
  * **restApiName** (<code>string</code>)  *No description* __*Optional*__
  * **restApiStageName** (<code>string</code>)  *No description* __*Optional*__
  * **restApiStageVariables** (<code>Map<string, string></code>)  *No description* __*Optional*__
  * **restApiThrottlingBurstLimit** (<code>number</code>)  *No description* __*Optional*__
  * **restApiThrottlingRateLimit** (<code>number</code>)  *No description* __*Optional*__
  * **restApiTracingEnabled** (<code>boolean</code>)  *No description* __*Optional*__



### Properties


Name | Type | Description 
-----|------|-------------
**api** | <code>[RestApi](#aws-cdk-aws-apigateway-restapi)</code> | <span></span>
**apiAvailabilityAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>
**apiClientErrorRateAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>
**apiLatencyP90sAlarm** | <code>[Alarm](#aws-cdk-aws-cloudwatch-alarm)</code> | <span></span>
**apiLogs** | <code>[LogGroup](#aws-cdk-aws-logs-loggroup)</code> | <span></span>
**apiRequestValidator** | <code>[RequestValidator](#aws-cdk-aws-apigateway-requestvalidator)</code> | <span></span>



## struct RestApiProps  <a id="softcomweb-ec-cdkpatterns-restapi-restapiprops"></a>






Name | Type | Description 
-----|------|-------------
**restApiAccessLogFormat**? | <code>[AccessLogFormat](#aws-cdk-aws-apigateway-accesslogformat)</code> | __*Optional*__
**restApiAuthorizer**? | <code>[Authorizer](#aws-cdk-aws-apigateway-authorizer)</code> | __*Optional*__
**restApiCanaryEnabled**? | <code>boolean</code> | __*Optional*__
**restApiCanaryTrafficPercent**? | <code>number</code> | __*Optional*__
**restApiDataTraceEnabled**? | <code>boolean</code> | __*Optional*__
**restApiDefaultIntegration**? | <code>[Integration](#aws-cdk-aws-apigateway-integration)</code> | __*Optional*__
**restApiLogLevel**? | <code>[MethodLoggingLevel](#aws-cdk-aws-apigateway-methodlogginglevel)</code> | __*Optional*__
**restApiLogsRetenion**? | <code>[RetentionDays](#aws-cdk-aws-logs-retentiondays)</code> | __*Optional*__
**restApiMethodOptions**? | <code>Map<string, [MethodDeploymentOptions](#aws-cdk-aws-apigateway-methoddeploymentoptions)></code> | __*Optional*__
**restApiMetricsEnabled**? | <code>boolean</code> | __*Optional*__
**restApiName**? | <code>string</code> | __*Optional*__
**restApiStageName**? | <code>string</code> | __*Optional*__
**restApiStageVariables**? | <code>Map<string, string></code> | __*Optional*__
**restApiThrottlingBurstLimit**? | <code>number</code> | __*Optional*__
**restApiThrottlingRateLimit**? | <code>number</code> | __*Optional*__
**restApiTracingEnabled**? | <code>boolean</code> | __*Optional*__



