# @softcomweb-ec/cdkpatterns_restapi

<!--BEGIN STABILITY BANNER-->

---
![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> CDK constructs to build REST APIs on API Gateway.

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/cdkpatterns_restapi
```

## How to Use

```js
import * as cdk from "@aws-cdk/core";
import * as cdkpatterns_restapi from "@softcomweb-ec/cdkpatterns_restapi";

// set per-endpoint integrations
const myserviceApi = new cdkpatterns_restapi.RestApi(this, "myservice", {

});
const myserviceApiFoosResource = myserviceApi.api.root.addResource("foos");
const myserviceApiFoosDescribeMethod = myserviceApiFoosResource.addMethod("GET", new apigateway.LambdaIntegration(handler));
const myserviceApiFoosCreateMethod = myserviceApiFoosResource.addMethod("POST", new apigateway.LambdaIntegration(handler));

// create an API with Lambda as the default integration
const lbrestapi = new cdkpatterns_restapi.LambdaBackedRestApi(this, "myservice", {
    restApiHandler: handler,
});
lbrestapi.api.root.addResource("foos").addMethod("GET")
```

## API Reference

See [API.md](API.md).