import * as cdk from "@aws-cdk/core";
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as lambda from "@aws-cdk/aws-lambda";
import * as logs from "@aws-cdk/aws-logs";

export interface RestApiProps {
  readonly restApiName?: string;
  readonly restApiStageName?: string;
  readonly restApiLogsRetenion?: logs.RetentionDays;
  readonly restApiLogLevel?: apigateway.MethodLoggingLevel;
  readonly restApiAccessLogFormat?: apigateway.AccessLogFormat;
  readonly restApiMetricsEnabled?: boolean;
  readonly restApiTracingEnabled?: boolean;
  readonly restApiDataTraceEnabled?: boolean;
  readonly restApiThrottlingBurstLimit?: number;
  readonly restApiThrottlingRateLimit?: number;
  readonly restApiMethodOptions?: Record<
    string,
    apigateway.MethodDeploymentOptions
  >;
  readonly restApiStageVariables?: Record<string, string>;
  readonly restApiDefaultIntegration?: apigateway.Integration;
  readonly restApiAuthorizer?: apigateway.Authorizer;

  readonly restApiCanaryEnabled?: true;
  readonly restApiCanaryTrafficPercent?: number;
}

export class RestApi extends cdk.Construct {
  public readonly api: apigateway.RestApi;
  public readonly apiRequestValidator: apigateway.RequestValidator;
  public readonly apiLogs: logs.LogGroup;
  public readonly apiClientErrorRateAlarm: cloudwatch.Alarm;
  public readonly apiAvailabilityAlarm: cloudwatch.Alarm;
  public readonly apiLatencyP90sAlarm: cloudwatch.Alarm;

  constructor(scope: cdk.Construct, id: string, props: RestApiProps) {
    super(scope, id);

    this.apiLogs = new logs.LogGroup(this, "apiLogs", {
      logGroupName: undefined,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      retention: props.restApiLogsRetenion || logs.RetentionDays.ONE_WEEK,
    });

    this.api = new apigateway.RestApi(this, "api", {
      deployOptions: {
        accessLogDestination: new apigateway.LogGroupLogDestination(
          this.apiLogs
        ),
        accessLogFormat:
          props.restApiAccessLogFormat ||
          apigateway.AccessLogFormat.jsonWithStandardFields(),
        loggingLevel:
          props.restApiLogLevel || apigateway.MethodLoggingLevel.INFO,
        dataTraceEnabled: props.restApiDataTraceEnabled,
        metricsEnabled: props.restApiMetricsEnabled,
        tracingEnabled: props.restApiTracingEnabled,
        throttlingBurstLimit: props.restApiThrottlingBurstLimit,
        throttlingRateLimit: props.restApiThrottlingRateLimit,
        methodOptions: props.restApiMethodOptions,
        variables: props.restApiStageVariables,
        stageName: props.restApiStageName,
      },
      endpointTypes: [apigateway.EndpointType.REGIONAL],
      restApiName: props.restApiName,
      defaultIntegration: props.restApiDefaultIntegration,
      defaultMethodOptions: {
        authorizer: props.restApiAuthorizer,
      },
    });

    if (props.restApiCanaryEnabled) {
      // https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-apigateway-stage-canarysetting.html
      (
        this.api.deploymentStage.node.defaultChild as apigateway.CfnStage
      ).addPropertyOverride("CanarySetting", {
        DeploymentId: this.api.latestDeployment?.deploymentId,
        PercentTraffic: props.restApiCanaryTrafficPercent || 100,
        StageVariableOverrides: {},
        UseStageCache: true,
      });
      new logs.LogGroup(this, "apiCanaryLogs", {
        logGroupName: `API-Gateway-Execution-Logs_${this.api.restApiId}/${this.api.deploymentStage.stageName}/Canary`,
        removalPolicy: cdk.RemovalPolicy.DESTROY,
        retention: props.restApiLogsRetenion || logs.RetentionDays.ONE_WEEK,
      });
    }

    this.apiRequestValidator = this.api.addRequestValidator("full", {
      validateRequestBody: true,
      validateRequestParameters: true,
    });

    this.apiClientErrorRateAlarm = new cloudwatch.Alarm(
      this,
      "apiClientErrorRateAlarm",
      {
        alarmName: undefined,
        evaluationPeriods: 1,
        metric: this.api.metricClientError({
          statistic: "Average", // error rate
        }),
        threshold: 0.4,
        alarmDescription: [
          "MEDIUM",
          `Rest API ${this.api.restApiId} exceeds a client rate of >40%`,
          "This results in degraded user experience for clients or indicate either an implementation error or attack",
          `Look at the CloudWatch log group ${this.apiLogs.logGroupName} and fix the implementation`,
        ].join(" | "),
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }
    );

    this.apiAvailabilityAlarm = new cloudwatch.Alarm(
      this,
      "apiAvailabilityAlarm",
      {
        alarmName: undefined,
        evaluationPeriods: 1,
        metric: this.api.metricServerError({
          statistic: "Average", // error rate
        }),
        threshold: 0.1,
        alarmDescription: [
          "HIGH",
          `Rest API ${this.api.restApiId} exceeds a server rate of >10%`,
          "This results in degraded availability for clients",
          `Look at the CloudWatch log group ${this.apiLogs.logGroupName} and fix the implementation`,
        ].join(" | "),
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }
    );

    this.apiLatencyP90sAlarm = new cloudwatch.Alarm(
      this,
      "apiLatencyP90Alarm",
      {
        alarmName: undefined,
        evaluationPeriods: 1,
        metric: this.api.metricLatency({
          statistic: "p90",
        }),
        threshold: 2000,
        alarmDescription: [
          "MEDIUM",
          `Rest API ${this.api.restApiId} exceeds a p90 latency of 2000ms`,
          "This results in degraded user experience for clients",
          `Look at the CloudWatch log group ${this.apiLogs.logGroupName} and fix the implementation. Consider rolling back the last deployment.`,
        ].join(" | "),
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }
    );
  }
}

export interface LambdaBackedRestApiProps extends RestApiProps {
  readonly restApiHandler: lambda.IFunction;
  readonly restApiHandlerInterationOptions?: apigateway.LambdaIntegrationOptions;
}

export class LambdaBackedRestApi extends RestApi {
  constructor(
    scope: cdk.Construct,
    id: string,
    props: LambdaBackedRestApiProps
  ) {
    super(scope, id, {
      restApiDefaultIntegration: new apigateway.LambdaIntegration(
        props.restApiHandler,
        props.restApiHandlerInterationOptions
      ),
      ...props,
    });
  }
}
