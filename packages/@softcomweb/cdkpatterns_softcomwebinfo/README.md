# @softcomweb-ec/cdkpatterns_softcomwebinfo

<!--BEGIN STABILITY BANNER-->

---
![cdk-constructs: Experimental](https://img.shields.io/badge/cdk--constructs-experimental-important.svg?style=for-the-badge)

> The APIs of higher level constructs in this module are experimental and under active development.
> They are subject to non-backward compatible changes or removal in any future version. These are
> not subject to the [Semantic Versioning](https://semver.org/) model and breaking changes will be
> announced in the release notes. This means that while you may use them, you may need to update
> your source code when upgrading to a newer version of this package.
---

<!--END STABILITY BANNER-->

> Common constants for working in the softcomweb ecosystem.

Features:

- Standard DNS records for Campus CIDRS as TXT records 
- Common AWS accounts to add to your allowlists (for IAM policies etc)
- softcomweb concepts like environments (live, develop, ...), cost tags
- AWS resource names for shared resources like
    - Global CloudTrail bucket

## Install

TypeScript/JavaScript:

```
npm i @softcomweb-ec/cdkpatterns_softcomwebinfo
```

## How to Use

```js
import * as cdk from "@aws-cdk/core";
import * as cdkpatterns_softcomwebinfo from "@softcomweb-ec/cdkpatterns_softcomwebinfo";

const stack = new cdk.Stack(new cdk.App(), "mystack", {
    env: {
        account: cdkpatterns_softcomwebinfo.AWS_ACCOUNT_ID_DNS,
    },
    // use case: bouncer Lambda to configure firewall access 
    // from softcomweb CIDRS (vpn, campus, spice)
    dnsResolveAllowList: [
        ...cdkpatterns_softcomwebinfo.softcomweb_CIDR_CAMPUS_RECORDS,
    ],
    // create a standard dns name with ".cloud.softcomweb.de"
    dnsName: `myapp.${cdkpatterns_ottinfo.softcomweb_CLOUD_DNS_ROOT_DOMAIN}`,
});

// add the environment tag
cdk.Tags.of(stack).add(cdkpatterns_softcomwebinfo.AWS_TAG_KEY_ENVIRONMENT, cdkpatterns_softcomwebinfo.softcomweb_ENVIRONMENT_LIVE);
```

## API Reference

See [API.md](API.md).