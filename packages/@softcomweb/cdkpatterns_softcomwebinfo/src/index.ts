// Important AWS account ids. NOTE: They are updated
// regularly but should be checked before using them.
export const AWS_ACCOUNT_ID_PAYER = "xx";
export const AWS_ACCOUNT_ID_PAYER_TEST = "xx";
export const AWS_ACCOUNT_ID_DNS = "xx";
export const AWS_ACCOUNT_ID_BACKUP = "xx";
export const AWS_ACCOUNT_ID_SECURITYHUB = "xx";
export const AWS_ACCOUNT_ID_LOGMON_LIVE = "xx";
export const AWS_ACCOUNT_ID_AWSGUARD_LIVE = "xx";
export const AWS_ACCOUNT_ID_AWSGUARD_NONLIVE = "xx";
export const AWS_ACCOUNT_ID_OPS_LIVE = "xx";
export const AWS_ACCOUNT_ID_OPS_NONLIVE = "xx";

// Global CloudTrail log bucket
export const SECURITYHUB_CLOUDTRAIL_LOGBUCKET_NAME = `de-SOFTCOMWEB-${AWS_ACCOUNT_ID_SECURITYHUB}-live-logs`;
// GuardDuty management account
export const SECURITYHUB_GUARDDUTY_MANAGEMENT_ACCOUNT =
  AWS_ACCOUNT_ID_SECURITYHUB;

// Static DNS records. From https://github.com/SOFTCOMWEB-ec/OPS_dns/blob/main/cf-templates/dns-cloud-SOFTCOMWEB-de-static-records.yaml
export const SOFTCOMWEB_CLOUD_DNS_ROOT_DOMAIN = "cloud.SOFTCOMWEB.de";

export const SOFTCOMWEB_CIDR_CAMPUS_ALL_DNS_RECORD =
  "campus." + SOFTCOMWEB_CLOUD_DNS_ROOT_DOMAIN;
export const SOFTCOMWEB_CIDR_CAMPUS_VPN_DNS_RECORD =
  "campus-vpn." + SOFTCOMWEB_CLOUD_DNS_ROOT_DOMAIN;
export const SOFTCOMWEB_CIDR_CAMPUS_WIFI_DNS_RECORD =
  "campus-wifi." + SOFTCOMWEB_CLOUD_DNS_ROOT_DOMAIN;
export const SOFTCOMWEB_CIDR_CAMPUS_SPICE_DNS_RECORD =
  "spice." + SOFTCOMWEB_CLOUD_DNS_ROOT_DOMAIN;
export const SOFTCOMWEB_CIDR_CAMPUS_RECORDS = [
  SOFTCOMWEB_CIDR_CAMPUS_ALL_DNS_RECORD,
  SOFTCOMWEB_CIDR_CAMPUS_VPN_DNS_RECORD,
  SOFTCOMWEB_CIDR_CAMPUS_WIFI_DNS_RECORD,
  SOFTCOMWEB_CIDR_CAMPUS_SPICE_DNS_RECORD,
];

// Common AWS tag keys
export const AWS_TAG_KEY_TEAM = "team";
export const AWS_TAG_KEY_VERTICAL = "vertical";
export const AWS_TAG_KEY_SERVICE = "service";
export const AWS_TAG_KEY_ENVIRONMENT = "environment";

// SOFTCOMWEB standard environments
export const SOFTCOMWEB_ENVIRONMENT_LIVE = "live";
export const SOFTCOMWEB_ENVIRONMENT_NONLIVE = "nonlive";
export const SOFTCOMWEB_ENVIRONMENT_DEVELOP = "develop";
export const SOFTCOMWEB_ENVIRONMENT_DEVELOPCI = "develop-ci";

// SOFTCOMWEB URLs
export const SOFTCOMWEB_WEBSHOP_DOMAIN = "SOFTCOMWEB.de";
export const SOFTCOMWEB_WEBSHOP_DOMAIN_NONLIVE = "develop.SOFTCOMWEB.de";
export const SOFTCOMWEB_API_DOMAIN = "api.SOFTCOMWEB.de";
export const SOFTCOMWEB_API_DOMAIN_NONLIVE = "api.develop.SOFTCOMWEB.de";

// OPS monitoring
export const OPS_MONITORING_AWS_REGION = "eu-west-1";
export const OPS_MONITORING_SNS_TOPIC_ARN_LIVE = `arn:aws:sns:${OPS_MONITORING_AWS_REGION}:${AWS_ACCOUNT_ID_OPS_LIVE}:de-SOFTCOMWEB-OPS-live-status`;
export const OPS_MONITORING_KMS_KEY_ARN_LIVE = `arn:aws:kms:${OPS_MONITORING_AWS_REGION}:${AWS_ACCOUNT_ID_OPS_LIVE}:alias/SNSMasterEncryptionKey`;
export const OPS_MONITORING_SNS_TOPIC_ARN_NONLIVE = `arn:aws:sns:${OPS_MONITORING_AWS_REGION}:${AWS_ACCOUNT_ID_OPS_NONLIVE}:de-SOFTCOMWEB-OPS-nonlive-status`;
export const OPS_MONITORING_KMS_KEY_ARN_NONLIVE = `arn:aws:kms:${OPS_MONITORING_AWS_REGION}:${AWS_ACCOUNT_ID_OPS_NONLIVE}:alias/SNSMasterEncryptionKey`;
