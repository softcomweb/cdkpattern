#!/usr/bin/env bash

set -eo pipefail

npm install
npm install -ws
npm run build -ws --if-exists
npm run package -ws --if-exists
npm run docgen -ws --if-exists
npm run release:npm -ws --if-exists