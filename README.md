# @softcomweb-ec/cdkpatterns

> A collection of useful AWS CDK patterns.

## Developing

- Each solution is developed as an NPM package and pushed to GitHub package registry (NPM, Maven)
- Each solution is developed using [JSII](https://aws.github.io/jsii) for multi-language support
- We support Java (Maven), Python (pip) and Node.js (NPM). Note that publishing Python packages is not yet supported
- We use [cdkdx](https://www.npmjs.com/package/cdkdx) for bundling all packages with JSII

Each NPM package resides in [packages/@softcomweb-ec](packages/@softcomweb-ec) and needs the following npm scripts:

- `build` runs `cdkdx build`
- `test` runs `cdkdx test`
- `lint` runs `cdkdx lint`
- `docgen` runs `cdkdx docgen` which creates a file called `API.md`

See [packages/@softcomweb-ec/cdkpatterns_starter](packages/@softcomweb-ec/cdkpatterns_starter) for a sample. Please adjust the `jsii` property accordingly to your package.

### Creating a new Package

To create a new package, first copy [packages/@softcomweb-ec/cdkpatterns_starter](packages/@softcomweb-ec/cdkpatterns_starter) and update `package.json`. The package name should match `cdkpatterns_{suffix}`, e.g. `cdkpatterns_myservice`. 

Notable changes are:

- `maintainers` property. this needs to be set to your e-mail address
- `jsii` property for publishing and compiling to other languages

Other steps include:

- Updating [CODEOWNERS](.github/CODEOWNERS)